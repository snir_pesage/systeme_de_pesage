<!DOCTYPE html Public "-//W3C//DTD XHTML 1.0 Strict //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml/" xml:lang="fr">
  <head>
    <title>Javascript || Copier/Coller</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" media="screen" type="text/css" title="Principale feuille de style" href="" />
  </head>
  
  <body>

  <table>
    <tr>
      <td><textarea id="texte-original"></textarea></td>
      <td><textarea id="texte-copie" readonly="readonly"></textarea></td>
    </tr>
    <tr>

      <td></td>
    </tr>
  </table>

  </body>
</html>








<!DOCTYPE html Public "-//W3C//DTD XHTML 1.0 Strict //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml/" xml:lang="fr">
  <head>
    <title>Javascript || Copier/Coller</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" media="screen" type="text/css" title="Principale feuille de style" href="" />

    <script type="text/javascript">
    var presse_papier = '';

    function copier(texte) {
      presse_papier = texte;
    }

    function coller() {
      document.getElementById('texte-copie').value = presse_papier;
    }
    </script>
  </head>
  
  <body>
  
  <table>
    <tr>

    </tr>
    <tr>
      <td><input type="button" value="Copier" onclick="copier(document.getElementById('texte-original').value);" /><input type="button" value="Coller" onclick="coller();" /></td>
      <td></td>
    </tr>
  </table>

  </body>
</html>