<?php 

session_start();
require 'class_Login.php'; 



$objUsers = new Users;

$username = "";
$errors = array();

if(!isset($_SERVER['HTTP_REFERER'])){
	  // redirect them to your desired location
    header('location: index.php');
    exit;
}
?>

<!DOCTYPE html>

<html>

<head>
<title> Changement de mot de passe </title>
<link rel="stylesheet" type="text/css" href="changePassword.css"> 
</head>

<body>

<br>
<header>

 <h1 class="title_connect"> Nouveau mot de passe <br> Afin de changer votre mot de passe compléter les champs</h1>
 
</header>

<br><br><br>

<section>
	<article>
	
		<div class="content">
		<div class="content2">
		<center><img class="circle" src="images/circle.png" alt="" /></center>
		
		<form class="form" action="modifiePassword.php" method="post">
		<br><br>
		<h2 class="form_connect"> Modifier mot de passe </h2> 
		
		<?php
		
		
		$db = $objUsers->connectionBD(); // Méthode permettant de se connecter a la base de donnée
		
		if (isset($_POST['modifiePassword'])) {
			
		$oldpassword = $db-> real_escape_string($_POST['oldpassword']);
		$password = $db -> real_escape_string($_POST['password']);
		$confirmpassword = $db-> real_escape_string($_POST['confirmpassword']);
		
		$objConditionMotDePasse = $objUsers->conditionMDP($oldpassword, $password, $confirmpassword);
		
		if (count($errors) == 0) {
		$objConditionMotDePasse = $objUsers->conditionMDP2($oldpassword, $password, $confirmpassword);
		}
		
		if (count($errors) == 0) {
			$objChangerMotDePasse = $objUsers->modifieMotDePasse($oldpassword, $password, $confirmpassword, $db);			
		}
		}
		
		?>
	
		 <center><input type="password" placeholder="Entrer votre mot de passe" name="oldpassword" /></center> <br>
		
		<center><input type="password" placeholder="Entrer un nouveau mot de passe" name="password" /></center> <br>
		
		<center><input type="password" placeholder="Confirmer le mot de passe" name="confirmpassword"/></center> <br> 
		
		<center><input type="submit" value="Confirmer" name="modifiePassword" class="btn" /></center> 	
			
			
		
		</div>
		</div>	
			
		</form>
			
	</article>
</section>


</body>

<footer>
<br><br><br><br><br>


<center>
<div class="content_footer">
<table class="tab_conseil">
<tr>

<td>
<img class="triangle" src="images/triangleJaune.png" alt="" />
</td>

<td>
	<p class="conseil"> <font color ="#ff0000"><strong> Attention :</strong> </font> le mot de passe doit contenir minimum 6 caractere.</red><br>
	<font color="green"><strong>Conseil :</strong></font> choisissez un mot de passe avec minimum un chiffre et un caractère spécial. <br> 
	<font color = "#ffbd0a"> Memoriser bien votre nouveau mot de passe !</font></p>
</td>

</tr>

</table>
</div>
</center>


</footer>

</html>