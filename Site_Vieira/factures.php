<?php 
session_start();
require 'class_Factures.php'; 

$objFactures = new Factures;

// si l'utilisateur n'est pas connecter il ne peut pas acceder a cette page

if (empty($_SESSION['username'])) {
	header('location: login.php');	
}

?>

<!DOCTYPE html>

<html>

<head>
<title> Factures </title>
<link rel="stylesheet" type="text/css" href="factures.css"> 
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  

</head>

<body>

<header>

 <input type="checkbox" id="btn">
 

 <label for="btn"><img src="images/menu_ico.png" class="btn_menu" alt=""></label>


<nav class="navegacion">
<ul class="menu">


<li>
<a href="camera.php">&nbsp CAMERA &nbsp </a>
</li>
 
 
 
 <li>
<a href="factures.php">&nbsp FACTURES &nbsp </a>
</li>



<li>
<a href="#">&nbsp HISTORIQUE  &nbsp </a></p>

<ul class="submenuHisto">
	<li><a href="historique_chargement.php"> CHARGEMENT </a></li>
	<li><a href="historique_test.php"> TEST </a></li>
</ul>
</li>



<li>
<a href="#">&nbsp GRAPHIQUE &nbsp </a></p>

<ul class="submenuGraph">
	<li><a href="#">BLÉ  </a></li>
	<li><a href="#">GRAVIER </a></li>
	<li><a href="#">TERRE </a></li>
	<li><a href="#">SABLE </a></li>
	<li><a href="#">CHARGEMENT </a></li>
</ul>
</li>



<li>
<a href="#">&nbsp PARAMETRE &nbsp </a></p>

<ul class="submenuSetting">
	<li><a href="modifiePassword.php"> CHANGER MOT DE PASSE </a></li>
	<li><a href="index.php?Deconnexion='1'"> DECONNEXION </a></li>
</ul>
</li>

</ul>
</nav>

</header>


<section>
	<article>
		<br><br>
		
<center><h1 class="factures_title"> FACTURES </h1>

<br>

	<a href="/projetClasse/modele_facture/Modele"> Telecharger modèle </a>
<br>
	<table>
	
		<tr class="title">
		<th> Les factures </th> 
		<th> téléchargement </th>
		<th> suppression </th>
		</tr>
	

	<br>
	
	<?php  
	$dir = "./factures/";
	
	//  si le dossier pointe existe
	if (is_dir($dir)) {
	
    // si il contient quelque chose
    if ($dh = opendir($dir)) {
		chdir($dir);
       // boucler tant que quelque chose est trouve
       array_multisort(array_map('basename', ($file = glob("*.*"))));
	   natsort($file) ;
	   $file = array_reverse($file);
	   foreach($file as $filename){
		
           // affiche le nom et le type si ce n'est pas un element du systeme
           if( $file != '.' && $file != '..') {
			   
			   
			   
			   ?>
			   
           <tr><td><?php echo $filename ;?></td>
		   <td><a href="/projetClasse/factures/<?php echo $filename;?>"> <img class="img_DL" src="images/DL.png" /></td>
		   
		   
		   <td> <form action="factures.php" method="post"> 
		   
		    <input id="image" type="image" width="25" height="25" src="images/delete.png" name="delete" value="<?php echo $filename ;?>" />
		   
		   
		   </form> </td>
		   
			
		   </tr>
		  <?php
           }
       }

       // on ferme la connection
       closedir($dh);
   }
} 

?>


</table>

	<!--
	<script language="javascript">
	
	a = confirm(" Etes vous sur de bien vouloir supprimer cette factures ? ")
	
	if (a == 1){ (" ")}
	if (a == 0){ alert(" Non ")}
	</script> !-->

	
	<?php 
	
	$upload = $objFactures -> upload();

?>





<?php

	if(isset($_POST['delete']))
	{
	
	$supprimer = $objFactures -> suppr();
	} 
?>


<br>

 <form action="" method="POST" enctype="multipart/form-data">
         <input type="file" name="ods" />
         <input type="submit"/>
      </form>

	  
	  

	  
</body>


</html>
