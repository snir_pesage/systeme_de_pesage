package snir.pesage.Conducteur;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

import snir.pesage.Database.BDDManager;
import snir.pesage.Database.InterfaceBDD;
import snir.pesage.Database.Utilisateur;
import snir.pesage.Database.UtilisateurTech;
import snir.pesage.R;

public class FinalChargement extends AppCompatActivity {

    private int nombre = 0;
    private int nombre2 = 0;


    private Boolean restant = true;

    private TextView temps;
    private TextView materiaux;
    private TextView quantite;
    private Button quitter;
    private String tempsRecu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_chargement);

        Intent intentTemps = getIntent();
        String tempsTotal = intentTemps.getStringExtra("temps");

        setTempsRecu(tempsTotal);

        temps = findViewById(R.id.textViewTemps);
        materiaux = findViewById(R.id.textViewMateriaux);
        quantite = findViewById(R.id.textViewQuantite);
        quitter = findViewById(R.id.buttonQuitter);


        recap();
        synchAuto();


    }

    private void recap() {
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        Utilisateur utilisateurFromBDD = new Utilisateur();
        utilisateurFromBDD = bdd.getLastUtilisateurEnvoyer(0);

        temps.setText(tempsRecu);

        materiaux.setText(utilisateurFromBDD.getMateriaux());


        quantite.setText(String.valueOf(utilisateurFromBDD.getChargeU()) + " kg chargé");



        bdd.close();
    }



    public void quitterApplication(View v){
        //android.os.Process.killProcess(android.os.Process.myPid());
        //System.exit(1);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            this.finishAffinity();
        }
    }



    private void synchAuto(){
        if (checkInternet() == true) synchro();
        else Snackbar.make(getWindow().getDecorView().getRootView(), "Connectez vous au wifi et redemarrez l'application !", Snackbar.LENGTH_INDEFINITE).show();
    }

    private boolean checkInternet() {
        boolean exists = false;

        try {
            SocketAddress sockaddr = new InetSocketAddress("192.168.9.9", 80);
            // Create an unbound socket
            Socket sock = new Socket();

            // This method will block no more than timeoutMs.
            // If the timeout occurs, SocketTimeoutException is thrown.
            int timeoutMs = 2000;   // 2 seconds
            sock.connect(sockaddr, timeoutMs);
            exists = true;
            sock.close();
        } catch (UnknownHostException e) {
            // Handle exception
        } catch (IOException e) {
            // Handle exception
        }
        return exists;
    }


    private void synchro(){
        BDDManager bdd =  new BDDManager(this);
        InterfaceBDD interfaceBDD = new InterfaceBDD();
        bdd.open();


        Utilisateur utilisateurFromBDD = new Utilisateur();
        Utilisateur utilisateurFromBDDSafe = new Utilisateur();
        utilisateurFromBDD = bdd.getLastUtilisateurEnvoyer(0);
        if (utilisateurFromBDD != null) utilisateurFromBDDSafe = utilisateurFromBDD;
        if (utilisateurFromBDD != null) nombre = utilisateurFromBDD.getId();

        UtilisateurTech utilisateurFromBDD2 = new UtilisateurTech();
        UtilisateurTech utilisateurFromBDDSafe2 = new UtilisateurTech();
        utilisateurFromBDD2 = bdd.getLastUtilisateurTechEnvoyer(0);
        if ( utilisateurFromBDD2 != null) utilisateurFromBDDSafe2 = utilisateurFromBDD2;
        if ( utilisateurFromBDD2 != null) nombre2 = utilisateurFromBDD2.getId();

        if ( nombre < nombre2) nombre = nombre2;

        for(int i = nombre; i > 0; i--){
            if (restant == true){
                utilisateurFromBDD = bdd.getLastUtilisateurEnvoyer(0);
                if (utilisateurFromBDD != null) {
                    nombre = utilisateurFromBDD.getId();

                    interfaceBDD.ecrireUtilisateur(utilisateurFromBDD);

                    bdd.supprimerUtilisateur(String.valueOf(nombre));
                }
                else {
                    restant = false;
                    i++;
                }

            }
            else {
                utilisateurFromBDD2 = bdd.getLastUtilisateurTechEnvoyer(0);
                if (utilisateurFromBDD2 != null) {
                    nombre2 = utilisateurFromBDD2.getId();
                    interfaceBDD.ecrireUtilisateurTech(utilisateurFromBDD2);
                    bdd.supprimerUtilisateurTech(String.valueOf(nombre2));
                }


            }

        }

        while(bdd.getLastUtilisateurEnvoyer(1) != null) {
            Utilisateur utilisateurFromBDD3 = new Utilisateur();
            utilisateurFromBDD3 = bdd.getLastUtilisateurEnvoyer(1);
            nombre = utilisateurFromBDD3.getId();
            bdd.supprimerUtilisateur(String.valueOf(nombre));
        }
        while(bdd.getLastUtilisateurTechEnvoyer(1) != null) {
            UtilisateurTech utilisateurFromBDD4 = new UtilisateurTech();
            utilisateurFromBDD4 = bdd.getLastUtilisateurTechEnvoyer(1);
            nombre = utilisateurFromBDD4.getId();
            bdd.supprimerUtilisateurTech(String.valueOf(nombre));
        }

        if (utilisateurFromBDD != null) {
            utilisateurFromBDDSafe.setEnvoyer(1);
            bdd.insertUtilisateur(utilisateurFromBDDSafe);
        }

        if ( utilisateurFromBDD2 != null) {
            utilisateurFromBDDSafe2.setEnvoyer(1);
            bdd.insertUtilisateurTech(utilisateurFromBDDSafe2);
        }


        bdd.close();
        Snackbar.make(getWindow().getDecorView().getRootView(), "Synchronisation réussi !", Snackbar.LENGTH_LONG).show();

    }

    private void setTempsRecu(String temps){
        this.tempsRecu = temps;
    }
}
