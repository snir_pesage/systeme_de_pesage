package snir.pesage.Technicien;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import snir.pesage.Database.BDDManager;
import snir.pesage.Conducteur.Chargement;
import snir.pesage.R;
import snir.pesage.Database.UtilisateurTech;

public class TestTechnicien extends AppCompatActivity {

    private Button buttonMoteur;
    private Button buttonElec;
    private Button buttonMesure;
    private Button buttonRelais;
    private Button buttonDemarrage;
    private String etatTech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_technicien);

        buttonMoteur = findViewById(R.id.buttonMoteur);
        buttonElec = findViewById(R.id.buttonElec);
        buttonMesure = findViewById(R.id.buttonMesure);
        buttonRelais = findViewById(R.id.buttonRelais);
        buttonDemarrage = findViewById(R.id.buttonDem);

    }

    public void openTestMoteur(View v){
        Intent intentTestMoteur = new Intent(this, TestMoteur.class);
        startActivity(intentTestMoteur);
    }

    public void openTestDemarrage(View v){
        Intent intentTestDemarrage = new Intent(this, TestDemarrage.class);
        startActivity(intentTestDemarrage);
    }

    public void openTestElec(View v){
        Intent intentTestElec = new Intent(this, TestElec.class);
        startActivity(intentTestElec);
    }

    public void openTestMesure(View v){
        Intent intentTestMesure = new Intent(this, TestMesure.class);
        startActivity(intentTestMesure);
    }

    public void openTestRelais(View v){
        Intent intentTestRelais = new Intent(this, TestRelais.class);
        startActivity(intentTestRelais);
    }

    public void openChargement(View v){
        Intent intentCharge = new Intent(this, Chargement.class);
        intentCharge.putExtra("etatTech", true);
        startActivity(intentCharge);
    }

}
