package snir.pesage.Bluetooth;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.macroyau.blue2serial.BluetoothDeviceListDialog;
import com.macroyau.blue2serial.BluetoothSerial;
import com.macroyau.blue2serial.BluetoothSerialListener;

import snir.pesage.Conducteur.MenuConduc;
import snir.pesage.R;
import snir.pesage.Technicien.MenuTech;

import static snir.pesage.R.layout.menu_terminal;

public class Connexion extends AppCompatActivity  implements BluetoothSerialListener, BluetoothDeviceListDialog.OnDeviceSelectedListener {

    private static final int REQUEST_ENABLE_BLUETOOTH = 1;



    private Button connexion;
    private Button continuer;
    private Button deconnexion;

    public static String message = "test";

    public Boolean etat;

    public BluetoothSerial bluetoothSerial;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

        connexion = findViewById(R.id.buttonConnexionBlue);
        deconnexion = findViewById(R.id.buttonDeconnexionBlue);
        continuer = findViewById(R.id.buttonContinuer);

        message = "";

        Intent etatTech = getIntent();
        Boolean etatRecup = etatTech.getBooleanExtra("choix", Boolean.parseBoolean(null));
        etat = etatRecup;

        Bluetooth.listener = this;
        bluetoothSerial = Bluetooth.get_instance(this).bluetoothSerial;
    }



    public void btnConnexion(View v){
        showDeviceListDialog();

    }

    public void btnDeconnexion(View v){

        bluetoothSerial.stop();
    }

    public void btnContinuer(View v){
        if( etat ){
            Intent intentMenu = new Intent(this, MenuConduc.class);
            startActivity(intentMenu);
        }
        else {
            Intent intentMenu = new Intent(this, MenuTech.class);
            startActivity(intentMenu);
        }
    }

    public void btnMenuBluetooth(View v){
            Intent intentOpenBluetoothSettings = new Intent();
            intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
            startActivity(intentOpenBluetoothSettings);
        }


    @Override
    protected void onStart() {
        super.onStart();

        // Check Connexion availability on the device and set up the Connexion adapter
        bluetoothSerial.setup();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Open a Connexion serial port and get ready to establish a connection
        if (bluetoothSerial.checkBluetooth() && bluetoothSerial.isBluetoothEnabled()) {
            if (!bluetoothSerial.isConnected()) {
                bluetoothSerial.start();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_ENABLE_BLUETOOTH:
                // Set up Connexion serial port when Connexion adapter is turned on
                if (resultCode == Activity.RESULT_OK) {
                    bluetoothSerial.setup();
                }
                break;
        }
    }

    private void updateBluetoothState() {
        // Get the current Connexion state
        final int state;
        if (bluetoothSerial != null)
            state = bluetoothSerial.getState();
        else
            state = BluetoothSerial.STATE_DISCONNECTED;

        // Display the current state on the app bar as the subtitle
        String subtitle;
        switch (state) {
            case BluetoothSerial.STATE_CONNECTING:
                subtitle = getString(R.string.status_connecting);
                continuer.setEnabled(false);
                deconnexion.setEnabled(false);
                continuer.setEnabled(false);
                break;
            case BluetoothSerial.STATE_CONNECTED:
                subtitle = getString(R.string.status_connected, bluetoothSerial.getConnectedDeviceName());
                continuer.setEnabled(false);
                deconnexion.setEnabled(true);
                continuer.setEnabled(true);
                Snackbar.make(getWindow().getDecorView().getRootView(), "Connexion réussi !", Snackbar.LENGTH_LONG).show();
                break;
            default:
                subtitle = getString(R.string.status_disconnected);
                connexion.setEnabled(true);
                deconnexion.setEnabled(false);
                continuer.setEnabled(true);// TODO: 28/05/2018

                break;
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(subtitle);
        }
    }

    private void showDeviceListDialog() {
        // Display dialog for selecting a remote Connexion device
        BluetoothDeviceListDialog dialog = new BluetoothDeviceListDialog(this);
        dialog.setOnDeviceSelectedListener(this);
        dialog.setTitle(R.string.paired_devices);
        dialog.setDevices(bluetoothSerial.getPairedDevices());
        dialog.showAddress(true);
        dialog.show();
    }

    /* Implementation of BluetoothSerialListener */

    @Override
    public void onBluetoothNotSupported() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.no_bluetooth)
                .setPositiveButton(R.string.action_quit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finish();
                    }
                })
                .setCancelable(false)
                .show();
    }

    @Override
    public void onBluetoothDisabled() {
        Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
    }

    @Override
    public void onBluetoothDeviceDisconnected() {

        updateBluetoothState();
    }

    @Override
    public void onConnectingBluetoothDevice() {
        updateBluetoothState();
    }

    @Override
    public void onBluetoothDeviceConnected(String name, String address) {

        updateBluetoothState();
    }

    @Override
    public void onBluetoothSerialRead(String message1) {

        message = message1;

    }

    @Override
    public void onBluetoothSerialWrite(String message) {

    }



    @Override
    public void onBluetoothDeviceSelected(BluetoothDevice device) {
        // Connect to the selected remote Connexion device
        bluetoothSerial.connect(device);

    }



}