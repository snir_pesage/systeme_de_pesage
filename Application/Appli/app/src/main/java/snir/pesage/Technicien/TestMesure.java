package snir.pesage.Technicien;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.macroyau.blue2serial.BluetoothSerial;

import snir.pesage.Bluetooth.Bluetooth;
import snir.pesage.Bluetooth.Connexion;
import snir.pesage.Database.BDDManager;
import snir.pesage.R;
import snir.pesage.Database.UtilisateurTech;

import static android.os.SystemClock.sleep;

public class TestMesure extends AppCompatActivity {

    protected boolean mbActive;
    protected ProgressBar barreCharg;

    private int i;
    private String p;
    private int nombre;
    private Boolean buttonSwitch = false;

    private BluetoothSerial bluetoothSerial;

    private Boolean mesure;
    private Boolean mesure2;

    private Button buttonMesure;
    private Button buttonMesureAV;

    private TextView recu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_mesure);

        buttonMesure = findViewById(R.id.buttonMesure);
        buttonMesureAV = findViewById(R.id.buttonMesureAV);
        recu = findViewById(R.id.textViewRecuMesure);

        barreCharg = findViewById(R.id.progressBarMesure);
        bluetoothSerial = Bluetooth.bluetoothSerial;

        checkUtil();

        new Thread(new Runnable() {
            public void run() {
                while (true){
                    runOnUiThread(new Runnable() {
                    public void run() {
                        recu.setText("Recu : " + Connexion.message);
                    }
                    });
                    if (i == 1) {


                        if ((p.equals(recu.getText().toString())) != true )
                            testMesureStart();
                            mbActive = false;
                    }
                    if (i == 2){


                        if ((p.equals(recu.getText().toString())) != true ){
                            testMesureStop();
                            testMesure();
                            mbActive = false;
                        }
                    }
                    if (i == 3){


                        if ((p.equals(recu.getText().toString())) != true ){
                            testMesureAVStart();
                            mbActive = false;
                        }
                    }
                    if (i == 4){


                        if ((p.equals(recu.getText().toString())) != true ){
                            testMesureAVStop();
                            testMesureAV();
                            mbActive = false;
                        }
                    }
                    p = recu.getText().toString();
                    sleep(100);
                }
            }
        }).start();

    }



    public void testMesure(View v) {
        if (mbActive == false) {

            if (buttonSwitch == true) {
                bluetoothSerial.write("stopMes", true);

                i = 2;


                buttonMesureAV.setEnabled(true);

                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonMesure.setText("Demarrer");
                        barreCharg.setVisibility(View.INVISIBLE);
                    }
                });


                mbActive = true;
                buttonSwitch = false;

            }else{

                mbActive = true;
                buttonMesureAV.setEnabled(false);

                bluetoothSerial.write("startMes", true);
                i = 1;

                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonMesure.setText("Arreter");
                        barreCharg.setVisibility(View.VISIBLE);
                    }
                });

                buttonSwitch = true;
            }


        }
    }

    public void testMesureAV(View v){
        if (mbActive == false) {

            if (buttonSwitch == true) {
                bluetoothSerial.write("stopEmptyMes", true);

                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonMesureAV.setText("Demarrer");
                        barreCharg.setVisibility(View.INVISIBLE);
                    }
                });

                i = 4;
                buttonMesure.setEnabled(true);
                mbActive = true;
                buttonSwitch = false;

            }else {

                mbActive = true;
                buttonMesure.setEnabled(false);
                bluetoothSerial.write("startEmptyMes", true);
                i = 3;
                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonMesureAV.setText("Arreter");
                        barreCharg.setVisibility(View.VISIBLE);
                    }
                });
                buttonSwitch = true;
            }


    }}

    public Boolean testMesureStart() {
        if ( Connexion.message == "OKStartMes" ) return mesure = true;
        return mesure = false;
    }

    public Boolean testMesureStop() {
        if ( Connexion.message == "OKStopMes" ) return mesure2 = true;
        return mesure2 = false;
    }

    public Boolean testMesureComplet() {
        if ( mesure == mesure2 == true ) return true;
        return false;
    }

    public Boolean testMesureAVStart() {
        if ( Connexion.message == "OKStartEmpty" ) return mesure = true;
        return mesure = false;
    }

    public Boolean testMesureAVStop() {
        if ( Connexion.message == "OKStopEmpty" ) return mesure2 = true;
        return mesure2 = false;
    }

    public Boolean testMesureAVComplet() {
        if ( mesure == mesure2 == true ) return true;
        return false;
    }

    public void testMesure(){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();

        if ( testMesureComplet() ) {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_mesure("OK");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);

        }
        else{
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_mesure("FAIL");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);
        }
        bdd.close();
    }

    public void testMesureAV(){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();

        if ( testMesureAVComplet() ) {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_mesureAV("OK");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);
        }
        else{
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_mesureAV("FAIL");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);
        }
        bdd.close();
    }


    private void checkUtil() {
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurFromBDD = new UtilisateurTech();
        utilisateurFromBDD = bdd.getLastUtilisateurTech("Technicien");
        if ( utilisateurFromBDD != null) nombre = utilisateurFromBDD.getId();
        bdd.close();
    }
}
