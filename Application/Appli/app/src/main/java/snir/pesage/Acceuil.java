package snir.pesage;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

import snir.pesage.Bluetooth.Connexion;
import snir.pesage.Database.BDDManager;
import snir.pesage.Database.InterfaceBDD;
import snir.pesage.Database.Utilisateur;
import snir.pesage.Database.UtilisateurTech;

public class Acceuil extends AppCompatActivity {

    private Button bouton;
    private Button buttonSynch;
    private RadioButton choix1;
    private RadioButton choix2;
    private EditText code;
    private TextView entrerCode;
    private int nombre = 0;
    private int nombre2 = 0;

    private Boolean restant = true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        bouton = findViewById(R.id.buttonConnexionBlue);
        buttonSynch = findViewById(R.id.buttonSynchro);
        choix1 = findViewById(R.id.radioButtonConducteur);
        choix2 = findViewById(R.id.radioButtonTechnicien);
        entrerCode= findViewById(R.id.textViewCode);
        code = findViewById(R.id.editTextCode);

        checkUtil();

    }



    public void choixConducteur(View v){
        choix2.setChecked(false);
        entrerCode.setVisibility(View.INVISIBLE);
        code.setVisibility(View.INVISIBLE);
    }

    public void choixTechnicien(View v){
        choix1.setChecked(false);
        entrerCode.setVisibility(View.VISIBLE);
        code.setVisibility(View.VISIBLE);
    }



    //Redirige sur la page selectionné
    public void boutonConnexion(View v) {
        if (choix1.isChecked()) openBluetoothConduc();
        if(choix2.isChecked()) {
            if (code.getText().toString().equals("3210")) openBluetoothTech();
            else Snackbar.make(v, "Erreur code !",Snackbar.LENGTH_SHORT).show();
        }
    }


    private void openBluetoothConduc() {
        Intent intentBluetooth = new Intent(this, Connexion.class);
        intentBluetooth.putExtra("choix", true);
        startActivity(intentBluetooth);
    }

    private void openBluetoothTech(){
       Intent intentBluetooth = new Intent(this, Connexion.class);
       intentBluetooth.putExtra("choix", false);
       startActivity(intentBluetooth);
    }

    public void bouttonSynch(View v){

        buttonSynch.setEnabled(false);
        synchro();
    }

    private void checkUtil() {
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        Utilisateur utilisateurFromBDD = new Utilisateur();
        utilisateurFromBDD = bdd.getLastUtilisateurEnvoyer(0);
        if (utilisateurFromBDD != null) nombre = utilisateurFromBDD.getId();

        UtilisateurTech utilisateurFromBDD2 = new UtilisateurTech();
        utilisateurFromBDD2 = bdd.getLastUtilisateurTechEnvoyer(0);
        if ( utilisateurFromBDD2 != null) nombre2 = utilisateurFromBDD2.getId();



        if (nombre > 3 || nombre2 > 3) Snackbar.make(getWindow().getDecorView().getRootView(), "Veuillez vous connecter au wifi du dépot.", Snackbar.LENGTH_INDEFINITE).show();

        if (nombre != 0 || nombre2 != 0) {

            if (checkInternet() != false) {
                buttonSynch.setVisibility(View.VISIBLE);

            }
        }

        bdd.close();
    }

    private boolean checkInternet() {
        boolean connexion = false;

        try {
            SocketAddress sockaddr = new InetSocketAddress("192.168.9.9", 80);

            Socket sock = new Socket();


            int timeout = 2000;   // 2 seconds
            sock.connect(sockaddr, timeout);
            connexion = true;
            sock.close();
        } catch (UnknownHostException e) {

       } catch (IOException e) {

        }
        return connexion;
    }


    private void synchro(){
        BDDManager bdd =  new BDDManager(this);
        InterfaceBDD interfaceBDD = new InterfaceBDD();
        bdd.open();


        Utilisateur utilisateurFromBDD = new Utilisateur();
        Utilisateur utilisateurFromBDDSafe = new Utilisateur();
        utilisateurFromBDD = bdd.getLastUtilisateurEnvoyer(0);
        if (utilisateurFromBDD != null) utilisateurFromBDDSafe = utilisateurFromBDD;
        if (utilisateurFromBDD != null) nombre = utilisateurFromBDD.getId();

        UtilisateurTech utilisateurFromBDD2 = new UtilisateurTech();
        UtilisateurTech utilisateurFromBDDSafe2 = new UtilisateurTech();
        utilisateurFromBDD2 = bdd.getLastUtilisateurTechEnvoyer(0);
        if ( utilisateurFromBDD2 != null) utilisateurFromBDDSafe2 = utilisateurFromBDD2;
        if ( utilisateurFromBDD2 != null) nombre2 = utilisateurFromBDD2.getId();

        if ( nombre < nombre2) nombre = nombre2;

        for(int i = nombre; i > 0; i--){
            if (restant == true){
                utilisateurFromBDD = bdd.getLastUtilisateurEnvoyer(0);
                if (utilisateurFromBDD != null) {
                    nombre = utilisateurFromBDD.getId();

                    interfaceBDD.ecrireUtilisateur(utilisateurFromBDD);

                    bdd.supprimerUtilisateur(String.valueOf(nombre));
                }
                else {
                    restant = false;
                    i++;
                }

            }
            else {
                utilisateurFromBDD2 = bdd.getLastUtilisateurTechEnvoyer(0);
                if (utilisateurFromBDD2 != null) {
                    nombre2 = utilisateurFromBDD2.getId();
                    interfaceBDD.ecrireUtilisateurTech(utilisateurFromBDD2);
                    bdd.supprimerUtilisateurTech(String.valueOf(nombre2));
                }


            }

        }

        while(bdd.getLastUtilisateurEnvoyer(1) != null) {
            Utilisateur utilisateurFromBDD3 = new Utilisateur();
            utilisateurFromBDD3 = bdd.getLastUtilisateurEnvoyer(1);
            nombre = utilisateurFromBDD3.getId();
            bdd.supprimerUtilisateur(String.valueOf(nombre));
        }
        while(bdd.getLastUtilisateurTechEnvoyer(1) != null) {
            UtilisateurTech utilisateurFromBDD4 = new UtilisateurTech();
            utilisateurFromBDD4 = bdd.getLastUtilisateurTechEnvoyer(1);
            nombre = utilisateurFromBDD4.getId();
            bdd.supprimerUtilisateurTech(String.valueOf(nombre));
        }

        if (utilisateurFromBDD != null) {
            utilisateurFromBDDSafe.setEnvoyer(1);
            bdd.insertUtilisateur(utilisateurFromBDDSafe);
        }

        if ( utilisateurFromBDD2 != null) {
            utilisateurFromBDDSafe2.setEnvoyer(1);
            bdd.insertUtilisateurTech(utilisateurFromBDDSafe2);
        }


        bdd.close();
        buttonSynch.setVisibility(View.INVISIBLE);
        Snackbar.make(getWindow().getDecorView().getRootView(), "Synchronisation réussi !", Snackbar.LENGTH_LONG).show();


    }


}


