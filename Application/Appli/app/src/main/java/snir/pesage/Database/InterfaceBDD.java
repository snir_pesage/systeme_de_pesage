package snir.pesage.Database;

import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Kevin on 05/03/2018.
 */

public class InterfaceBDD extends AsyncTask<String, String, String> {

    private JSONParser jsonParser = new JSONParser();
    private Gson gson = new Gson();
    private int erreur = 0;
    private String url_insertUtilisateur = "http://192.168.9.9/innotronic/create_utilisateur.php";
    private String url_insertUtilisateurTech = "http://192.168.9.9/innotronic/create_utilisateurTech.php";
    private String url_getUtilisateur = "http://192.168.9.9/innotronic/get_utilisateur.php";



    public InterfaceBDD()  {
    }

    @Override
    protected String doInBackground(String... strings) {
        return null;
    }

    public int getErreur() {
        return this.erreur;
    }

    public List<UtilisateurTech> lireHistoriqueTechnicien() {
        JSONErreur jsonErreur = null;
        JSONObject json = null;

        try {
            HashMap<String, String> params = new HashMap();
            json = this.jsonParser.makeHttpRequest(this.url_getUtilisateur, "POST", params);
            if(json != null) {
                JSONListeTech jsonListeTech = (JSONListeTech)this.gson.fromJson(json.toString(), JSONListeTech.class);
                if(jsonListeTech.success == 0) {
                    this.erreur = 3;
                    jsonErreur = (JSONErreur)this.gson.fromJson(json.toString(), JSONErreur.class);
                    return null;
                }

                return jsonListeTech.infos;
            }
        } catch (Exception var6) {
            var6.printStackTrace();
            this.erreur = 1;
        }

        return null;
    }

    public List<Utilisateur> lireHistoriqueChargement() {
        JSONErreur jsonErreur = null;
        JSONObject json = null;

        try {
            HashMap<String, String> params = new HashMap();
            json = this.jsonParser.makeHttpRequest(this.url_getUtilisateur, "GET", params);
            if(json != null) {
                JSONListeChar jsonListeChar = (JSONListeChar)this.gson.fromJson(json.toString(), JSONListeChar.class);
                if(jsonListeChar.success == 0) {
                    this.erreur = 3;
                    jsonErreur = (JSONErreur)this.gson.fromJson(json.toString(), JSONErreur.class);
                    return null;
                }

                return jsonListeChar.infos;
            }
        } catch (Exception var6) {
            var6.printStackTrace();
            this.erreur = 1;
        }

        return null;
    }


    /**
     *
     * @param utilisateur
     * @return
     */
    public int ecrireUtilisateur(Utilisateur utilisateur) {
        LinkedHashMap<String, String> params = new LinkedHashMap();


        params.put("nom", utilisateur.getNom());
        params.put("societe", utilisateur.getSociete());
        params.put("type", utilisateur.getType_utilisateur());
        params.put("materiaux", utilisateur.getMateriaux());
        params.put("chargeU", String.valueOf(utilisateur.getChargeU()));
        params.put("date", utilisateur.getDate());
        params.put("mac", utilisateur.getMac());
        JSONObject json = this.jsonParser.makeHttpRequest(this.url_insertUtilisateur, "POST", params);
        return this.erreur;
    }

    public int ecrireUtilisateurTech(UtilisateurTech utilisateur) {
        LinkedHashMap<String, String> params = new LinkedHashMap();

        params.put("nom", utilisateur.getNom());
        params.put("type", utilisateur.getType_utilisateur());
        params.put("test_moteur_ON", utilisateur.getTest_moteur_ON());
        params.put("test_moteur_OFF", utilisateur.getTest_moteur_OFF());
        params.put("test_elec_tension", utilisateur.getTest_elec_tension());
        params.put("test_elec_intensite", utilisateur.getTest_elec_intensite());
        params.put("test_elec_puissance", utilisateur.getTest_elec_puissance());
        params.put("test_elec_energie", utilisateur.getTest_elec_energie());
        params.put("test_demarrage_ON", utilisateur.getTest_demarrage_ON());
        params.put("test_demarrage_OFF", utilisateur.getTest_demarrage_OFF());
        params.put("test_demarrage_AU", utilisateur.getTest_demarrage_AU());
        params.put("test_mesure", utilisateur.getTest_mesure());
        params.put("test_mesureAV", utilisateur.getTest_mesureAV());
        params.put("test_relais1", utilisateur.getTest_relais1());
        params.put("test_relais2", utilisateur.getTest_relais2());
        params.put("test_relais3", utilisateur.getTest_relais3());
        params.put("test_relais4", utilisateur.getTest_relais4());
        params.put("date", utilisateur.getDate());
        params.put("mac", utilisateur.getMac());
        JSONObject json = this.jsonParser.makeHttpRequest(this.url_insertUtilisateurTech, "POST", params);
        System.out.print(json);
        return this.erreur;
    }
}
