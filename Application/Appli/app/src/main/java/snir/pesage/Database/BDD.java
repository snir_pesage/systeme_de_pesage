package snir.pesage.Database;

/**
 * Created by Kevin on 08/02/2018.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDD extends SQLiteOpenHelper {

    private static final String TABLE_IDENTIFIANT = "table_identifiant";
    private static final String TABLE_TECHNICIEN = "table_technicien";



    private static final String CREATE_BDD = "CREATE TABLE table_identifiant ( id INT PRIMARY KEY NOT NULL, nom TEXT, " +
            "societe TEXT, type TEXT, materiaux TEXT, chargeU NUMBER, date TEXT, envoyer NUMBER, mac TEXT)";

    private static final String CREATE_BDD_TECH = "CREATE TABLE table_technicien ( id INT PRIMARY KEY NOT NULL, nom TEXT, type TEXT," +
            " test_moteur_ON TEXT, test_moteur_OFF TEXT, test_elec_tension TEXT, test_elec_intensite TEXT," +
            " test_elec_puissance TEXT, test_elec_energie TEXT, test_demarrage_ON TEXT, test_demarrage_OFF TEXT," +
            " test_demarrage_AU TEXT, test_mesure TEXT, test_mesureAV TEXT, test_relais1 TEXT, test_relais2 TEXT," +
            " test_relais3 TEXT, test_relais4 TEXT, date TEXT, envoyer NUMBER, mac TEXT); ";

    public BDD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //on crée la table à partir de la requête écrite dans la variable CREATE_BDD
        db.execSQL(CREATE_BDD);
        db.execSQL(CREATE_BDD_TECH);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //On peut faire ce qu'on veut ici moi j'ai décidé de supprimer la table et de la recréer
        //comme ça lorsque je change la version les id repartent de 0
        db.execSQL("DROP TABLE " + TABLE_IDENTIFIANT + ";");
        db.execSQL("DROP TABLE " + TABLE_TECHNICIEN + ";");
        onCreate(db);
    }

}
