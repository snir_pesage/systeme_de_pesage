package snir.pesage.Technicien;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.macroyau.blue2serial.BluetoothSerial;

import snir.pesage.Bluetooth.Bluetooth;
import snir.pesage.Bluetooth.Connexion;
import snir.pesage.Database.BDDManager;
import snir.pesage.Database.UtilisateurTech;
import snir.pesage.R;

import static android.os.SystemClock.sleep;

public class TestMoteur extends AppCompatActivity {

    private Thread thread ;

    protected boolean mbActive = false;
    protected ProgressBar barreCharg;

    private int nombre;
    private int i = 3;
    private String p;

    private Button buttonMarche;
    private Button buttonArret;


    private BluetoothSerial bluetoothSerial;

    private TextView recu;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_moteur);
        thread = new Thread();

        buttonMarche = findViewById(R.id.buttonMoteurOn);
        buttonArret = findViewById(R.id.buttonMoteurOFF);

        barreCharg = findViewById(R.id.progressBarMoteur);
        recu = findViewById(R.id.textViewRecu);

        bluetoothSerial = Bluetooth.bluetoothSerial;

        checkUtil();


        new Thread(new Runnable() {
            public void run() {
                while (true){
                    runOnUiThread(new Runnable() {
                        public void run() {
                            recu.setText("Recu : " + Connexion.message);
                        }
                    });

                    if (i == 1) {


                        if ((p.equals(recu.getText().toString())) != true ) {
                            testMoteurOn();
                            barreCharg.setVisibility(View.INVISIBLE);
                        }


                    }
                    if (i == 0) {


                        if ((p.equals(recu.getText().toString())) != true) {
                            testMoteurOff();
                            barreCharg.setVisibility(View.INVISIBLE);

                        }
                    }
                    p = recu.getText().toString();
                    sleep(100);
            }
            }
        }).start();
    }


    public void testMarche(View v){

        if (mbActive == false) {

            runOnUiThread(new Runnable() {
                public void run() {
                    barreCharg.setVisibility(View.VISIBLE);
                }
            });
            mbActive = true;
            buttonArret.setEnabled(true);
            buttonMarche.setEnabled(false);
            bluetoothSerial.write("Startmot", true);
            i = 1;


        }

    }

    public void testArret(View v) {
        if (mbActive == false) {


            runOnUiThread(new Runnable() {
                public void run() {
                    barreCharg.setVisibility(View.INVISIBLE);
                }
            });

            mbActive = true;
            buttonArret.setEnabled(false);
            buttonMarche.setEnabled(true);
            bluetoothSerial.write("Stopmot", true);
            i = 0;

        }
    }

    public void testMoteurOn(){
        if (mbActive == true){
        BDDManager bdd = new BDDManager(this);
        bdd.open();
        if ( Connexion.message.equals("OKStartmot") ) {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_moteur_ON("OK");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);

        }
        else {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_moteur_ON("FAIL");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);
        }
        bdd.close();
        mbActive = false;
    }}

    public void testMoteurOff(){
        if (mbActive == true){
        BDDManager bdd = new BDDManager(this);
        bdd.open();
        if (Connexion.message.equals("OKStopmot")) {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_moteur_OFF("OK");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);

        }
        else {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_moteur_OFF("FAIL");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);
        }

        bdd.close();
        mbActive = false;
    }}

    private void checkUtil() {
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurFromBDD = new UtilisateurTech();
        utilisateurFromBDD = bdd.getLastUtilisateurTech("Technicien");
        if ( utilisateurFromBDD != null) nombre = utilisateurFromBDD.getId();
        bdd.close();
    }

}
