package snir.pesage.Technicien;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.macroyau.blue2serial.BluetoothSerial;

import snir.pesage.Bluetooth.Bluetooth;
import snir.pesage.Bluetooth.Connexion;
import snir.pesage.Database.BDDManager;
import snir.pesage.Database.UtilisateurTech;
import snir.pesage.R;

import static android.os.SystemClock.sleep;

public class TestDemarrage extends AppCompatActivity {

    private ProgressBar barreCharg;
    protected boolean mbActive;


    private int i = 3;
    private String p;
    private int nombre;


    private Button marche;
    private Button arret;
    private Button arretU;
    private TextView recu;

    private BluetoothSerial bluetoothSerial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_demarrage);

        marche = findViewById(R.id.buttonDemarrageON);
        arret = findViewById(R.id.buttonDemarrageOFF);
        arretU = findViewById(R.id.buttonDemarrageAU);
        barreCharg = findViewById(R.id.progressBarDemar);

        recu = findViewById(R.id.textViewRecuDem);

        bluetoothSerial = Bluetooth.bluetoothSerial;
        checkUtil();

        new Thread(new Runnable() {
            public void run() {
                while (true){
                    runOnUiThread(new Runnable() {
                        public void run() {
                            recu.setText("Recu : " + Connexion.message);
                        }
                    });
                    if (i == 1) {

                        //Connexion.message = "";
                        if ((p.equals(recu.getText().toString())) != true )
                            testDemarrage();
                        runOnUiThread(new Runnable() {
                            public void run() {
                                barreCharg.setVisibility(View.INVISIBLE);
                            }
                        });
                        //buttonMarche.setEnabled(true);
                        //barreCharg.setVisibility(View.INVISIBLE);
                        //mbActive = false;

                    }
                    if (i == 0){

                        //Connexion.message = "";
                        if ((p.equals(recu.getText().toString())) != true )
                            testArret();
                        runOnUiThread(new Runnable() {
                            public void run() {
                                barreCharg.setVisibility(View.INVISIBLE);
                            }
                        //buttonMarche.setEnabled(true);
                        //barreCharg.setVisibility(View.INVISIBLE);
                        //mbActive = false;

                    });}
                    if (i == 2) {
                        if ((p.equals(recu.getText().toString())) != true )
                            testArretU();
                            runOnUiThread(new Runnable() {
                                              public void run() {
                                                  barreCharg.setVisibility(View.INVISIBLE);
                                              }
                            });}
                    p = recu.getText().toString();
                    sleep(100);
                }
            }
        }).start();

    }





    public void testMarche(View v){
        if (mbActive == false) {

            //barreCharg.setVisibility(View.VISIBLE);
            mbActive = true;
            arret.setEnabled(true);
            marche.setEnabled(false);
            arretU.setEnabled(true);
            bluetoothSerial.write("Start", true);
            i = 1;

            runOnUiThread(new Runnable() {
                public void run() {
                    barreCharg.setVisibility(View.VISIBLE);
                }
            });

        }
    }

    public void testArret(View v){
        if (mbActive == false) {

            //barreCharg.setVisibility(View.VISIBLE);
            mbActive = true;
            arret.setEnabled(true);
            marche.setEnabled(true);
            bluetoothSerial.write("Stop", true);

            i = 0;

            runOnUiThread(new Runnable() {
                public void run() {
                    barreCharg.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public void testArretU(View v){
        if (mbActive == false) {



            mbActive = true;
            arret.setEnabled(true);
            arretU.setEnabled(true);
            marche.setEnabled(true);
            bluetoothSerial.write("EmergStop", true);
            i = 2;
            runOnUiThread(new Runnable() {
                public void run() {
                    barreCharg.setVisibility(View.VISIBLE);
                }
            });

        }
    }

    public void testDemarrage(){
        if (mbActive == true){
        BDDManager bdd = new BDDManager(this);
        bdd.open();
        if ( Connexion.message.equals("OK")) {

            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_demarrage_ON("OK");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);

        }
        else {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_demarrage_ON("FAIL");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);
        }
        bdd.close();
            mbActive = false;
        }}


    public void testArret(){
        if (mbActive == true){
        BDDManager bdd = new BDDManager(this);
        bdd.open();

        if ( Connexion.message.equals("OKStop") ) {

            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_demarrage_OFF("OK");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);

        }
        else{

            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_demarrage_OFF("FAIL");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);
        }
        bdd.close();
        mbActive = false;
    }}


    public void testArretU(){
        if (mbActive == true){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();

        if ( Connexion.message.equals("OKEmergStop")) {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_demarrage_AU("OK");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);
        }
        else{
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_demarrage_AU("FAIL");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(nombre, utilisateurTech);
        }
        bdd.close();
        mbActive = false;
        }}


    private void checkUtil() {
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurFromBDD = new UtilisateurTech();
        utilisateurFromBDD = bdd.getLastUtilisateurTech("Technicien");
        if ( utilisateurFromBDD != null) nombre = utilisateurFromBDD.getId();
        bdd.close();
    }
}
