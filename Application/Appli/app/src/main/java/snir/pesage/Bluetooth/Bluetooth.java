package snir.pesage.Bluetooth;

import android.app.Application;
import android.content.Context;

import com.macroyau.blue2serial.BluetoothSerial;
import com.macroyau.blue2serial.BluetoothSerialListener;

/**
 * Created by Kevin on 19/03/2018.
 */

public class Bluetooth implements BluetoothSerialListener{


    private static Bluetooth _instance = new Bluetooth();


    public static BluetoothSerial bluetoothSerial;
    public static BluetoothSerialListener listener;

    public static Bluetooth get_instance(Context context){
        bluetoothSerial = new BluetoothSerial(context, listener);
        return _instance;
    }


    @Override
    public void onBluetoothNotSupported() {

    }

    @Override
    public void onBluetoothDisabled() {

    }

    @Override
    public void onBluetoothDeviceDisconnected() {

    }

    @Override
    public void onConnectingBluetoothDevice() {

    }

    @Override
    public void onBluetoothDeviceConnected(String name, String address) {

    }

    @Override
    public void onBluetoothSerialRead(String message) {

    }

    @Override
    public void onBluetoothSerialWrite(String message) {
        // Print the outgoing message on the terminal screen

    }

    /* Implementation of BluetoothDeviceListDialog.OnDeviceSelectedListener */

}
