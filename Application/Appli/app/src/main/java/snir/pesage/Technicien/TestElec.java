package snir.pesage.Technicien;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.macroyau.blue2serial.BluetoothSerial;

import snir.pesage.Bluetooth.Bluetooth;
import snir.pesage.Bluetooth.Connexion;
import snir.pesage.Database.BDDManager;
import snir.pesage.Database.UtilisateurTech;
import snir.pesage.R;

import static android.os.SystemClock.sleep;

public class TestElec extends AppCompatActivity {


    protected boolean mbActive;
    protected ProgressBar barreCharg;

    private int idBDD;
    private int i;

    private TextView resultatTension;
    private TextView resultatIntensite;
    private TextView resultatPuissance;
    private TextView resultatEnergie;

    private BluetoothSerial bluetoothSerial;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_elec);

        resultatTension = findViewById(R.id.textViewResultatTension);
        resultatIntensite = findViewById(R.id.textViewResultatIntensite);
        resultatPuissance = findViewById(R.id.textViewResultatPuissance);
        resultatEnergie = findViewById(R.id.textViewResultatEnergie);

        barreCharg = findViewById(R.id.progressBarElec);

        bluetoothSerial = Bluetooth.bluetoothSerial;

        checkUtil();


        new Thread(new Runnable() {
            public void run() {
                while (true){

                    if (i == 1) {


                        if (Connexion.message.equals("reset") != true) {
                            i = 0;
                            testTension();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    resultatTension.setText("Resultat : " + Connexion.message);
                                    barreCharg.setVisibility(View.INVISIBLE);
                                }
                            });
                            Connexion.message = "reset";

                        }

                    }
                    if (i == 2) {


                        if (Connexion.message.equals("reset") != true) {
                            i = 0;
                            testIntensite();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    resultatIntensite.setText("Resultat : " + Connexion.message);
                                    barreCharg.setVisibility(View.INVISIBLE);
                                }
                            });
                            Connexion.message = "reset";


                        }
                    }
                    if (i == 3) {


                        if (Connexion.message.equals("reset") != true ) {
                            i = 0;
                            testPuissance();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    resultatPuissance.setText("Resultat : " + Connexion.message);
                                    barreCharg.setVisibility(View.INVISIBLE);
                                }
                            });
                            Connexion.message = "reset";


                        }
                    }
                    if (i == 4) {


                        if (Connexion.message.equals("reset") != true) {
                            i = 0;
                            testEnergie();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    resultatEnergie.setText("Resultat : " + Connexion.message);
                                    barreCharg.setVisibility(View.INVISIBLE);
                                }
                            });
                            Connexion.message = "reset";


                        }
                    }

                    sleep(200);
                }
            }
        }).start();

    }




    public void buttonTension(View v){
        if (mbActive == false) {

            runOnUiThread(new Runnable() {
                public void run() {

                    barreCharg.setVisibility(View.VISIBLE);
                }
            });

            mbActive = true;
            Connexion.message = "reset";
            bluetoothSerial.write("Tension", true);
            i = 1;

        }

    }

    public void buttonIntensite(View v){
        if (mbActive == false) {


            runOnUiThread(new Runnable() {
                public void run() {

                    barreCharg.setVisibility(View.VISIBLE);
                }
            });

            mbActive = true;
            Connexion.message = "reset";
            bluetoothSerial.write("Intensity", true);
            i = 2;


        }

    }

    public void buttonPuissance(View v){
        if (mbActive == false) {

            runOnUiThread(new Runnable() {
                public void run() {

                    barreCharg.setVisibility(View.VISIBLE);
                }
            });

            mbActive = true;
            bluetoothSerial.write("Power", true);
            i = 3;

        }

    }

    public void buttonEnergie(View v){
        if (mbActive == false) {

            runOnUiThread(new Runnable() {
                public void run() {

                    barreCharg.setVisibility(View.VISIBLE);
                }
            });

            mbActive = true;
            Connexion.message = "reset";
            bluetoothSerial.write("Energy", true);
            i = 4;

        }
    }

    public void testTension(){
        BDDManager bdd = new BDDManager(this);
        bdd.open();
        if ( Connexion.message.equals("") != true ) {

            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_elec_tension(Connexion.message);
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(idBDD, utilisateurTech);
            resultatTension.setText("Resultat " + utilisateurTech.getTest_elec_tension());

        }
        else {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_elec_tension("FAIL");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(idBDD, utilisateurTech);
            resultatTension.setText("Resultat " + utilisateurTech.getTest_elec_tension());
        }
        bdd.close();
    }

    public void testIntensite(){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();

        if ( Connexion.message.equals("") != true ) {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_elec_intensite(Connexion.message);
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(idBDD, utilisateurTech);
            resultatIntensite.setText("Resultat " + utilisateurTech.getTest_elec_intensite());

        }
        else {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_elec_intensite("FAIL");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(idBDD, utilisateurTech);
            resultatIntensite.setText("Resultat " + utilisateurTech.getTest_elec_intensite());
        }
        bdd.close();
    }

    public void testPuissance(){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();

        if ( Connexion.message.equals("") != true ) {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_elec_puissance(Connexion.message);
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(idBDD, utilisateurTech);
            resultatPuissance.setText("Resultat " + utilisateurTech.getTest_elec_puissance());
            bdd.close();
        }
        else{
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_elec_puissance("FAIL");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(idBDD, utilisateurTech);
            resultatPuissance.setText("Resultat " + utilisateurTech.getTest_elec_puissance());
        }
    }

    public void testEnergie(){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();

        if ( Connexion.message.equals("") != true ) {
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_elec_energie(Connexion.message);
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(idBDD, utilisateurTech);
            resultatEnergie.setText("Resultat " + utilisateurTech.getTest_elec_energie());

        }
        else{
            UtilisateurTech utilisateurTech = new UtilisateurTech();
            utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
            utilisateurTech.setTest_elec_energie("FAIL");
            utilisateurTech.setEnvoyer(0);
            bdd.updateUtilisateurTech(idBDD, utilisateurTech);
            resultatEnergie.setText("Resultat " + utilisateurTech.getTest_elec_energie());
        }
        bdd.close();
    }

    private void checkUtil() {
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurFromBDD = new UtilisateurTech();
        utilisateurFromBDD = bdd.getLastUtilisateurTech("Technicien");
        if ( utilisateurFromBDD != null) idBDD = utilisateurFromBDD.getId();
        bdd.close();
    }
}
