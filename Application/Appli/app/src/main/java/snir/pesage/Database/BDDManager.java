package snir.pesage.Database;

/**
 * Created by Kevin on 08/02/2018.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class BDDManager {

    private static final int VERSION_BDD = 2;
    private static final String NOM_BDD = "utilisateur.db";

    private static final String TABLE_IDENTIFIANT = "table_identifiant";
    private static final String COL_ID = "id";
    private static final int NUM_COL_ID = 0;
    private static final String COL_NOM = "NOM";
    private static final int NUM_COL_NOM = 1;
    private static final String COL_SOCIETE = "SOCIETE";
    private static final int NUM_COL_SOCIETE = 2;
    private static final String COL_TYPE = "TYPE";
    private static final int NUM_COL_TYPE = 3;
    private static final String COL_MATERIAUX = "MATERIAUX";
    private static final int NUM_COL_MATERIAUX = 4;
    private static final String COL_CHARGEU= "CHARGEU";
    private static final int NUM_COL_CHARGEU = 5;
    private static final String COL_DATE= "DATE";
    private static final int NUM_COL_DATE = 6;
    private static final String COL_ENVOYER = "ENVOYER";
    private static final int NUM_COL_ENVOYER = 7;
    private static final String COL_MAC = "MAC";
    private static final int NUM_COL_MAC = 8;

    private static final String TABLE_IDENTIFIANT1 = "table_technicien";
    private static final String COL_ID1 = "id";
    private static final int NUM_COL_ID1 = 0;
    private static final String COL_NOM1 = "NOM";
    private static final int NUM_COL_NOM1 = 1;
    private static final String COL_TYPE1 = "TYPE";
    private static final int NUM_COL_TYPE1 = 2;
    private static final String COL_TEST_MOTEUR_ON = "TEST_MOTEUR_ON";
    private static final int NUM_COL_TEST_MOTEUR_ON = 3;
    private static final String COL_TEST_MOTEUR_OFF = "TEST_MOTEUR_OFF";
    private static final int NUM_COL_TEST_MOTEUR_OFF = 4;
    private static final String COL_TEST_ELEC_TENSION = "TEST_ELEC_TENSION";
    private static final int NUM_COL_TEST_ELEC_TENSION = 5;
    private static final String COL_TEST_ELEC_INTENSITE = "TEST_ELEC_INTENSITE";
    private static final int NUM_COL_TEST_ELEC_INTENSITE = 6;
    private static final String COL_TEST_ELEC_PUISSANCE = "TEST_ELEC_PUISSANCE";
    private static final int NUM_COL_TEST_ELEC_PUISSANCE = 7;
    private static final String COL_TEST_ELEC_ENERGIE = "TEST_ELEC_ENERGIE";
    private static final int NUM_COL_TEST_ELEC_ENERGIE = 8;
    private static final String COL_TEST_DEMARRAGE_ON = "TEST_DEMARRAGE_ON";
    private static final int NUM_COL_TEST_DEMARRAGE_ON = 9;
    private static final String COL_TEST_DEMARRAGE_OFF = "TEST_DEMARRAGE_OFF";
    private static final int NUM_COL_TEST_DEMARRAGE_OFF = 10;
    private static final String COL_TEST_DEMARRAGE_AU = "TEST_DEMARRAGE_AU";
    private static final int NUM_COL_TEST_DEMARRAGE_AU = 11;
    private static final String COL_TEST_MESURE = "TEST_MESURE";
    private static final int NUM_COL_TEST_MESURE = 12;
    private static final String COL_TEST_MESUREAV = "TEST_MESUREAV";
    private static final int NUM_COL_TEST_MESUREAV = 13;
    private static final String COL_TEST_RELAIS1 = "TEST_RELAIS1";
    private static final int NUM_COL_TEST_RELAIS1 = 14;
    private static final String COL_TEST_RELAIS2 = "TEST_RELAIS2";
    private static final int NUM_COL_TEST_RELAIS2 = 15;
    private static final String COL_TEST_RELAIS3 = "TEST_RELAIS3";
    private static final int NUM_COL_TEST_RELAIS3 = 16;
    private static final String COL_TEST_RELAIS4 = "TEST_RELAIS4";
    private static final int NUM_COL_TEST_RELAIS4 = 17;
    private static final String COL_DATE1= "DATE";
    private static final int NUM_COL_DATE1 = 18;
    private static final String COL_ENVOYER1 = "ENVOYER";
    private static final int NUM_COL_ENVOYER1 = 19;
    private static final String COL_MAC1 = "MAC";
    private static final int NUM_COL_MAC1 = 20;


    private SQLiteDatabase bdd;

    private BDD maBaseSQLite;

    public BDDManager(Context context){
        //On crée la BDD et sa table
        maBaseSQLite = new BDD(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = maBaseSQLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    public long insertUtilisateur(Utilisateur utilisateur){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_ID, utilisateur.getId());
        values.put(COL_NOM, utilisateur.getNom());
        values.put(COL_SOCIETE, utilisateur.getSociete());
        values.put(COL_TYPE, utilisateur.getType_utilisateur());
        values.put(COL_MATERIAUX, utilisateur.getMateriaux());
        values.put(COL_CHARGEU, utilisateur.getChargeU());
        values.put(COL_DATE, utilisateur.getDate());
        values.put(COL_ENVOYER, utilisateur.getEnvoyer());
        values.put(COL_MAC, utilisateur.getMac());
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_IDENTIFIANT, null, values);
    }

    public long insertUtilisateurTech(UtilisateurTech utilisateurTech){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_ID1, utilisateurTech.getId());
        values.put(COL_NOM1, utilisateurTech.getNom());
        values.put(COL_TYPE1, utilisateurTech.getType_utilisateur());
        values.put(COL_TEST_MOTEUR_ON, utilisateurTech.getTest_moteur_ON());
        values.put(COL_TEST_MOTEUR_OFF, utilisateurTech.getTest_moteur_OFF());
        values.put(COL_TEST_ELEC_TENSION, utilisateurTech.getTest_elec_tension());
        values.put(COL_TEST_ELEC_INTENSITE, utilisateurTech.getTest_elec_intensite());
        values.put(COL_TEST_ELEC_PUISSANCE, utilisateurTech.getTest_elec_puissance());
        values.put(COL_TEST_ELEC_ENERGIE, utilisateurTech.getTest_elec_energie());
        values.put(COL_TEST_DEMARRAGE_ON, utilisateurTech.getTest_demarrage_ON());
        values.put(COL_TEST_DEMARRAGE_OFF, utilisateurTech.getTest_demarrage_OFF());
        values.put(COL_TEST_DEMARRAGE_AU, utilisateurTech.getTest_demarrage_AU());
        values.put(COL_TEST_MESURE, utilisateurTech.getTest_mesure());
        values.put(COL_TEST_MESUREAV, utilisateurTech.getTest_mesureAV());
        values.put(COL_TEST_RELAIS1, utilisateurTech.getTest_relais1());
        values.put(COL_TEST_RELAIS2, utilisateurTech.getTest_relais2());
        values.put(COL_TEST_RELAIS3, utilisateurTech.getTest_relais3());
        values.put(COL_TEST_RELAIS4, utilisateurTech.getTest_relais4());
        values.put(COL_DATE1, utilisateurTech.getDate());
        values.put(COL_ENVOYER1, utilisateurTech.getEnvoyer());
        values.put(COL_MAC, utilisateurTech.getMac());
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_IDENTIFIANT1, null, values);
    }

    public int updateUtilisateur(int id, Utilisateur utilisateur){

        ContentValues values = new ContentValues();
        values.put(COL_NOM, utilisateur.getNom());
        values.put(COL_SOCIETE, utilisateur.getSociete());
        values.put(COL_TYPE, utilisateur.getType_utilisateur());
        values.put(COL_MATERIAUX, utilisateur.getMateriaux());
        values.put(COL_CHARGEU, utilisateur.getChargeU());
        values.put(COL_DATE, utilisateur.getDate());
        values.put(COL_ENVOYER, utilisateur.getEnvoyer());
        values.put(COL_MAC, utilisateur.getMac());
        return bdd.update(TABLE_IDENTIFIANT, values, COL_ID + " = " + id, null);
    }

    public int updateUtilisateurTech(int id, UtilisateurTech utilisateurTech){

        ContentValues values = new ContentValues();
        values.put(COL_ID1, utilisateurTech.getId());
        values.put(COL_NOM1, utilisateurTech.getNom());
        values.put(COL_TYPE1, utilisateurTech.getType_utilisateur());
        values.put(COL_TEST_MOTEUR_ON, utilisateurTech.getTest_moteur_ON());
        values.put(COL_TEST_MOTEUR_OFF, utilisateurTech.getTest_moteur_OFF());
        values.put(COL_TEST_ELEC_TENSION, utilisateurTech.getTest_elec_tension());
        values.put(COL_TEST_ELEC_INTENSITE, utilisateurTech.getTest_elec_intensite());
        values.put(COL_TEST_ELEC_PUISSANCE, utilisateurTech.getTest_elec_puissance());
        values.put(COL_TEST_ELEC_ENERGIE, utilisateurTech.getTest_elec_energie());
        values.put(COL_TEST_DEMARRAGE_ON, utilisateurTech.getTest_demarrage_ON());
        values.put(COL_TEST_DEMARRAGE_OFF, utilisateurTech.getTest_demarrage_OFF());
        values.put(COL_TEST_DEMARRAGE_AU, utilisateurTech.getTest_demarrage_AU());
        values.put(COL_TEST_MESURE, utilisateurTech.getTest_mesure());
        values.put(COL_TEST_MESUREAV, utilisateurTech.getTest_mesureAV());
        values.put(COL_TEST_RELAIS1, utilisateurTech.getTest_relais1());
        values.put(COL_TEST_RELAIS2, utilisateurTech.getTest_relais2());
        values.put(COL_TEST_RELAIS3, utilisateurTech.getTest_relais3());
        values.put(COL_TEST_RELAIS4, utilisateurTech.getTest_relais4());
        values.put(COL_DATE1, utilisateurTech.getDate());
        values.put(COL_ENVOYER1, utilisateurTech.getEnvoyer());
        values.put(COL_MAC, utilisateurTech.getMac());
        return bdd.update(TABLE_IDENTIFIANT1, values, COL_ID1 + " = " + id, null);
    }



    public Utilisateur getLastUtilisateur(String type){

        Cursor c = bdd.query(TABLE_IDENTIFIANT, new String[] {COL_ID, COL_NOM, COL_SOCIETE, COL_TYPE, COL_MATERIAUX, COL_CHARGEU, COL_DATE, COL_ENVOYER, COL_MAC},
                COL_TYPE + " LIKE \"" + type +"\"", null, null, null, COL_ID +" DESC", "1");
        return cursorToUtilisateur(c);
    }

    public Utilisateur getLastUtilisateurEnvoyer(int envoyer){

        Cursor c = bdd.query(TABLE_IDENTIFIANT, new String[] {COL_ID, COL_NOM, COL_SOCIETE, COL_TYPE, COL_MATERIAUX, COL_CHARGEU, COL_DATE, COL_ENVOYER, COL_MAC}, COL_ENVOYER + " LIKE \"" + envoyer +"\"", null, null, null, COL_ID +" DESC", "1");
        return cursorToUtilisateur(c);
    }

    public UtilisateurTech getLastUtilisateurTech(String type){

        Cursor c = bdd.query(TABLE_IDENTIFIANT1, new String[] {COL_ID1, COL_NOM1, COL_TYPE1, COL_TEST_MOTEUR_ON, COL_TEST_MOTEUR_OFF, COL_TEST_ELEC_TENSION,
                COL_TEST_ELEC_INTENSITE, COL_TEST_ELEC_PUISSANCE, COL_TEST_ELEC_ENERGIE, COL_TEST_DEMARRAGE_ON, COL_TEST_DEMARRAGE_OFF,
                COL_TEST_DEMARRAGE_AU, COL_TEST_MESURE, COL_TEST_MESUREAV, COL_TEST_RELAIS1, COL_TEST_RELAIS2, COL_TEST_RELAIS3, COL_TEST_RELAIS4, COL_DATE1, COL_ENVOYER1, COL_MAC1}, COL_TYPE1 + " LIKE \"" + type +"\"", null, null, null, COL_ID1 +" DESC", "1");
        return cursorToUtilisateurTech(c);
    }

    public UtilisateurTech getLastUtilisateurTechEnvoyer(int envoyer){

        Cursor c = bdd.query(TABLE_IDENTIFIANT1, new String[] {COL_ID1, COL_NOM1, COL_TYPE1, COL_TEST_MOTEUR_ON, COL_TEST_MOTEUR_OFF, COL_TEST_ELEC_TENSION,
                COL_TEST_ELEC_INTENSITE, COL_TEST_ELEC_PUISSANCE, COL_TEST_ELEC_ENERGIE, COL_TEST_DEMARRAGE_ON, COL_TEST_DEMARRAGE_OFF,
                COL_TEST_DEMARRAGE_AU, COL_TEST_MESURE, COL_TEST_MESUREAV, COL_TEST_RELAIS1, COL_TEST_RELAIS2, COL_TEST_RELAIS3, COL_TEST_RELAIS4, COL_DATE1, COL_ENVOYER1, COL_MAC1}, COL_ENVOYER1 + " LIKE \"" + envoyer +"\"", null, null, null, COL_ID1 +" DESC", "1");
        return cursorToUtilisateurTech(c);
    }

    public void supprimerUtilisateur(String id) {
        bdd.delete(TABLE_IDENTIFIANT, COL_ID + " = ?", new String[] {id});
    }

    public void supprimerUtilisateurTech(String id) {
        bdd.delete(TABLE_IDENTIFIANT1, COL_ID + " = ?", new String[] {id});
    }


    //Cette méthode permet de convertir un cursor en un livre
    private Utilisateur cursorToUtilisateur(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();

        Utilisateur utilisateur = new Utilisateur();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        utilisateur.setId(c.getInt(NUM_COL_ID));
        utilisateur.setNom(c.getString(NUM_COL_NOM));
        utilisateur.setSociete(c.getString(NUM_COL_SOCIETE));
        utilisateur.setType_utilisateur(c.getString(NUM_COL_TYPE));
        utilisateur.setMateriaux(c.getString(NUM_COL_MATERIAUX));
        utilisateur.setChargeU(c.getInt(NUM_COL_CHARGEU));
        utilisateur.setDate(c.getString(NUM_COL_DATE));
        utilisateur.setEnvoyer(c.getInt(NUM_COL_ENVOYER));
        utilisateur.setMac(c.getString(NUM_COL_MAC));
        //On ferme le cursor
        c.close();


        return utilisateur;
    }

    private UtilisateurTech cursorToUtilisateurTech(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();

        UtilisateurTech utilisateurTech = new UtilisateurTech();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        utilisateurTech.setId(c.getInt(NUM_COL_ID1));
        utilisateurTech.setNom(c.getString(NUM_COL_NOM1));
        utilisateurTech.setType_utilisateur(c.getString(NUM_COL_TYPE1));
        utilisateurTech.setTest_moteur_ON(c.getString(NUM_COL_TEST_MOTEUR_ON));
        utilisateurTech.setTest_moteur_OFF(c.getString(NUM_COL_TEST_MOTEUR_OFF));
        utilisateurTech.setTest_elec_tension(c.getString(NUM_COL_TEST_ELEC_TENSION));
        utilisateurTech.setTest_elec_intensite(c.getString(NUM_COL_TEST_ELEC_INTENSITE));
        utilisateurTech.setTest_elec_puissance(c.getString(NUM_COL_TEST_ELEC_PUISSANCE));
        utilisateurTech.setTest_elec_energie(c.getString(NUM_COL_TEST_ELEC_ENERGIE));
        utilisateurTech.setTest_demarrage_ON(c.getString(NUM_COL_TEST_DEMARRAGE_ON));
        utilisateurTech.setTest_demarrage_OFF(c.getString(NUM_COL_TEST_DEMARRAGE_OFF));
        utilisateurTech.setTest_demarrage_AU(c.getString(NUM_COL_TEST_DEMARRAGE_AU));
        utilisateurTech.setTest_mesure(c.getString(NUM_COL_TEST_MESURE));
        utilisateurTech.setTest_mesureAV(c.getString(NUM_COL_TEST_MESUREAV));
        utilisateurTech.setTest_relais1(c.getString(NUM_COL_TEST_RELAIS1));
        utilisateurTech.setTest_relais2(c.getString(NUM_COL_TEST_RELAIS2));
        utilisateurTech.setTest_relais3(c.getString(NUM_COL_TEST_RELAIS3));
        utilisateurTech.setTest_relais4(c.getString(NUM_COL_TEST_RELAIS4));
        utilisateurTech.setDate(c.getString(NUM_COL_DATE1));
        utilisateurTech.setEnvoyer(c.getInt(NUM_COL_ENVOYER1));
        utilisateurTech.setMac(c.getString(NUM_COL_MAC1));
        //On ferme le cursor
        c.close();


        return utilisateurTech;
    }

}