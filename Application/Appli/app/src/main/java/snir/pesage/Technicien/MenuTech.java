package snir.pesage.Technicien;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.NetworkInterface;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import snir.pesage.Database.BDDManager;
import snir.pesage.Database.UtilisateurTech;
import snir.pesage.R;

public class MenuTech extends AppCompatActivity {

    private EditText textNom;
    private Button buttonTest;
    private Button buttonHisto;
    private Button buttonSauv;
    private TextView texteSauv;
    private int nombre;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_tech);

        textNom = findViewById(R.id.editTextNomTech);
        buttonTest = findViewById(R.id.buttonTest);

        buttonSauv = findViewById(R.id.buttonSauvTech);
        texteSauv = findViewById(R.id.textViewSauvTech);

        checkUtil();

    }


    public void allerTest(View v){
            if (textNom.getText().toString().trim().length() != 0) {
                sauvegardeNom();
                openTest();
            }
            else Snackbar.make(v, "Entrer un nom !", Snackbar.LENGTH_LONG).show();
    }

    public void chargeSauvegarde(View v){
        loadNom();
    }


    private void sauvegardeNom(){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        UtilisateurTech utilisateurTech = new UtilisateurTech(nombre + 1,textNom.getText().toString(), "Technicien","","","","","","","","","","","","","","","",dateFormat.format(date),1,getMac());


        bdd.insertUtilisateurTech(utilisateurTech);
        bdd.close();

    }

    private void checkUtil() {
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurFromBDD = new UtilisateurTech();
        utilisateurFromBDD = bdd.getLastUtilisateurTech("Technicien");
        if ( utilisateurFromBDD != null) nombre = utilisateurFromBDD.getId();

        if (utilisateurFromBDD != null){
            texteSauv.setVisibility(View.VISIBLE);
            buttonSauv.setVisibility(View.VISIBLE);
        }
        bdd.close();
    }

    private void loadNom() {
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurTech = new UtilisateurTech();
        utilisateurTech = bdd.getLastUtilisateurTech("Technicien");

        textNom.setText(utilisateurTech.getNom());
        bdd.close();
    }

    private void openTest(){
        Intent intentTest = new Intent(this, TestTechnicien.class);
        startActivity(intentTest);
    }



    public static String getMac() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

}
