package snir.pesage.Conducteur;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.macroyau.blue2serial.BluetoothSerial;

import java.net.NetworkInterface;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import snir.pesage.Bluetooth.Bluetooth;
import snir.pesage.Bluetooth.Connexion;
import snir.pesage.Database.BDDManager;
import snir.pesage.Database.Utilisateur;
import snir.pesage.Database.UtilisateurTech;
import snir.pesage.R;

import static android.os.SystemClock.sleep;

public class Chargement extends AppCompatActivity {



    protected static final int TIMER_RUNTIME = 10000;

    private int attente = 0;
    private int i;
    private String p;
    int charge;


    protected boolean mbActive;
    protected ProgressBar barreCharg;


    private BluetoothSerial bluetoothSerial;


    private RadioButton choix1;
    private RadioButton choix2;
    private TextView poidsAV;
    private TextView poidsM;
    private TextView chargeU;
    private TextView kg1;
    private TextView kg2;
    private TextView kg3;
    private EditText textPoidsAV;
    private EditText textPoidsM;
    private EditText textChargeU;
    private int nombre = 0;
    private int nombre2 = 0;
    private int etatDemarrage = 0;
    private Button demarrer;
    private Button pause;
    private Button arretU;



    private boolean etatChoix;




    private Boolean testEtat;
    private Temps temps = new Temps();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chargement);

        Intent etatTech = getIntent();
        Boolean etat = etatTech.getBooleanExtra("etatTech", Boolean.parseBoolean(null));

        setTestEtat(etat);


        barreCharg = findViewById(R.id.progressBarChargement);
        choix1 = findViewById(R.id.radioButtonVehicule);
        choix2 = findViewById(R.id.radioButtonMarchandise);
        poidsAV = findViewById(R.id.textViewPAV);
        poidsM = findViewById(R.id.textViewPM);
        chargeU = findViewById(R.id.textViewChargeU);
        kg1 = findViewById(R.id.textViewKg1);
        kg2 = findViewById(R.id.textViewKg2);
        kg3 = findViewById(R.id.textViewKg3);
        textPoidsAV = findViewById(R.id.editTextPAV);
        textPoidsM = findViewById(R.id.editTextPM);
        textChargeU = findViewById(R.id.editTextCU);

        demarrer = findViewById(R.id.buttonDemarrer);
        pause = findViewById(R.id.buttonPause);
        arretU = findViewById(R.id.buttonArretUrgence);

        bluetoothSerial = Bluetooth.bluetoothSerial;

        checkUtil();


        final Thread timerThread = new Thread(){
            @Override
            public void run(){
                mbActive = false;
                try{
                    attente = 0;
                    while(attente <= TIMER_RUNTIME){
                        sleep(200);

                        if (i == 1) {


                            if (Connexion.message.equals("OKload2")) {
                                i = 2;

                                Connexion.message = "reset";
                                trameChargementMasse();
                                Snackbar.make(getWindow().getDecorView().getRootView(), "Debut du chargemnt.", Snackbar.LENGTH_LONG).show();
                            }

                        }
                        if (i == 2){


                            if (Connexion.message.equals("OKmass")){

                                trameChargementStart();
                                Connexion.message = "reset";
                                i = 3;

                            }
                        }
                        if (i == 3){


                            if (Connexion.message.equals("OKStartLoading")){

                                etatDemarrage = 1;
                                Connexion.message = "reset";
                                i = 4;



                            }

                        }
                        if (i == 4) {


                            if (Connexion.message.equals("Recap")) {

                                openFinal();



                            }
                        }
                        if(mbActive){
                            updateAttente();

                        }
                    }

                } catch(InterruptedException e){

                }

            }
        };
        timerThread.start();


    }


    private void updateAttente() {
        if (mbActive == true) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    barreCharg.setVisibility(View.VISIBLE);
                }


            });
        }
        else {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    barreCharg.setVisibility(View.INVISIBLE);
                }


            });
        }
    }

    public void clickDemarrer(View v) {

        if (etatChoix == true) {
            if ((textPoidsAV.getText().toString().trim().length() == 0) || (textPoidsM.getText().toString().trim().length() == 0)) {
                Snackbar.make(v, "Erreur ! Entrer les bonnes valeurs (Pas négatives ni nulles).", Snackbar.LENGTH_LONG).show();
            } else {
                if ((Integer.parseInt(textPoidsAV.getText().toString()) > 0) && (Integer.parseInt(textPoidsM.getText().toString()) > 0) && (Integer.parseInt(textPoidsAV.getText().toString()) < 44000)
                        && (Integer.parseInt(textPoidsM.getText().toString()) < 44000) && Integer.parseInt(textPoidsAV.getText().toString()) < Integer.parseInt(textPoidsM.getText().toString())) {

                    if (etatDemarrage != 1) {

                        int poidsAVtest = Integer.parseInt(textPoidsAV.getText().toString());
                        int poidsMtest = Integer.parseInt(textPoidsM.getText().toString());
                        charge = poidsMtest - poidsAVtest;

                        chargementConducVehicule(charge);
                        bluetoothSerial.write("Load2", true);

                        i = 1;

                        mbActive = true;
                        demarrer.setEnabled(false);
                        pause.setEnabled(true);
                        arretU.setEnabled(true);

                    } else {
                        bluetoothSerial.write("ResumeLoading", true);
                        etatDemarrage = 1;
                        mbActive = true;
                        demarrer.setEnabled(false);
                        pause.setEnabled(true);
                        arretU.setEnabled(true);
                    }


                }else {
                    Snackbar.make(v, "Erreur ! Le poids doit etre compris entre 1 et 44 000 Kg.", Snackbar.LENGTH_LONG).show();
                }


            }
        } else {
            if ((textChargeU.getText().toString().trim().length() == 0)) {
                Snackbar.make(v, "Erreur ! Entrer une bonne valeur (Pas négative ni nulle).", Snackbar.LENGTH_LONG).show();
            } else {
                if (Integer.parseInt(textChargeU.getText().toString()) > 0 && Integer.parseInt(textChargeU.getText().toString()) < 44000) {
                    if (etatDemarrage != 1) {
                        charge = Integer.parseInt(textChargeU.getText().toString());
                        chargementConducUtile(charge);
                        bluetoothSerial.write("Load2", true);

                        i = 1;

                        mbActive = true;
                        demarrer.setEnabled(false);
                        pause.setEnabled(true);
                        arretU.setEnabled(true);
                    } else {
                        bluetoothSerial.write("ResumeLoading", true);
                        etatDemarrage = 1;
                        mbActive = true;
                        demarrer.setEnabled(false);
                        pause.setEnabled(true);
                        arretU.setEnabled(true);

                    }
                }else{
                    Snackbar.make(v, "Erreur ! Le poids doit etre compris entre 1 et 44 000 Kg.", Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }



    public void chargementConducUtile(int charge){

        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        if(getTestEtat() != true) {

            Utilisateur utilisateurFromBDD = bdd.getLastUtilisateur("Conducteur");
            Utilisateur utilisateur = new Utilisateur(nombre, utilisateurFromBDD.getNom(), utilisateurFromBDD.getSociete(), "Conducteur",
                    bluetoothSerial.getConnectedDeviceName(), charge, dateFormat.format(date),0, getMac());
            utilisateur.setEnvoyer(0);
            bdd.updateUtilisateur(nombre, utilisateur);
            temps.reset();
        }
        else {

            Utilisateur utilisateur = new Utilisateur(nombre+1, nomTech(), "", "Technicien",
                    bluetoothSerial.getConnectedDeviceName(), Integer.parseInt(textChargeU.getText().toString()), dateFormat.format(date),0, getMac());
            utilisateur.setEnvoyer(0);
            bdd.insertUtilisateur(utilisateur);
            temps.reset();
        }
        bdd.close();

    }

    public void chargementConducVehicule(int charge){

        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        if(getTestEtat() != true) {
            Utilisateur utilisateurFromBDD = bdd.getLastUtilisateur("Conducteur");
            Utilisateur utilisateur = new Utilisateur(nombre, utilisateurFromBDD.getNom(), utilisateurFromBDD.getSociete(), "Conducteur",
                    bluetoothSerial.getConnectedDeviceName(), charge, dateFormat.format(date),0, getMac());
            utilisateur.setEnvoyer(0);
            bdd.updateUtilisateur(nombre, utilisateur);

            temps.reset();
        }
        else {
            Utilisateur utilisateurFromBDD = bdd.getLastUtilisateur("Technicien");
            Utilisateur utilisateur = new Utilisateur(nombre+1, nomTech(), "", "Technicien",
                    bluetoothSerial.getConnectedDeviceName(), charge, dateFormat.format(date),0, getMac());
            utilisateur.setEnvoyer(0);
            bdd.insertUtilisateur(utilisateur);

            temps.reset();
        }
        bdd.close();

    }



    public void clickPause(View v){
        bluetoothSerial.write("StopLoading", true);
        mbActive = false;
        updateAttente();
        demarrer.setEnabled(true);
        pause.setEnabled(false);
        arretU.setEnabled(true);
    }

    public void clickAU(View v){
        bluetoothSerial.write("EmergStop", true);
        mbActive = false;
        updateAttente();
        demarrer.setEnabled(true);
        pause.setEnabled(false);
        arretU.setEnabled(true);
    }


    public void choixVehicule(View v){
        choix2.setChecked(false);
        textPoidsAV.setVisibility(View.VISIBLE);
        textPoidsM.setVisibility(View.VISIBLE);
        poidsAV.setVisibility(View.VISIBLE);
        poidsM.setVisibility(View.VISIBLE);
        kg1.setVisibility(View.VISIBLE);
        kg2.setVisibility(View.VISIBLE);

        textChargeU.setText("");
        textPoidsAV.setText("");
        textPoidsM.setText("");
        etatChoix = true;
        textChargeU.setVisibility(View.INVISIBLE);
        chargeU.setVisibility(View.INVISIBLE);
        kg3.setVisibility(View.INVISIBLE);

        demarrer.setEnabled(true);

    }

    public void choixMarchandise(View v){
        choix1.setChecked(false);
        textPoidsAV.setVisibility(View.INVISIBLE);
        textPoidsM.setVisibility(View.INVISIBLE);
        poidsAV.setVisibility(View.INVISIBLE);
        poidsM.setVisibility(View.INVISIBLE);
        kg1.setVisibility(View.INVISIBLE);
        kg2.setVisibility(View.INVISIBLE);


        textChargeU.setText("");
        textPoidsAV.setText("");
        textPoidsM.setText("");
        etatChoix = false;
        textChargeU.setVisibility(View.VISIBLE);
        chargeU.setVisibility(View.VISIBLE);
        kg3.setVisibility(View.VISIBLE);

        demarrer.setEnabled(true);


    }

    private void checkUtil() {
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        Utilisateur utilisateurFromBDD = new Utilisateur();
        utilisateurFromBDD = bdd.getLastUtilisateurEnvoyer(1);
        if (utilisateurFromBDD != null) nombre = utilisateurFromBDD.getId();

        Utilisateur utilisateurFromBDD2 = new Utilisateur();
        utilisateurFromBDD2 = bdd.getLastUtilisateurEnvoyer(0);
        if (utilisateurFromBDD2 != null) nombre2 = utilisateurFromBDD2.getId();

        if ( nombre < nombre2) nombre = nombre2;

        bdd.close();
    }

    private String nomTech(){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurTech = new UtilisateurTech();
        utilisateurTech = bdd.getLastUtilisateurTechEnvoyer(1);
        bdd.close();
        return utilisateurTech.getNom();
    }

    private void setTestEtat(Boolean etat){
        this.testEtat = etat;
    }

    private Boolean getTestEtat(){
        return testEtat;
    }

    public static String getMac() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    public void openFinal(){
        Intent intent = new Intent(this, FinalChargement.class);
        intent.putExtra("temps", temps.getElapsedTimeString());
        startActivity(intent);
    }


    public void trameChargementMasse(){
        bluetoothSerial.write(String.valueOf(charge), true);
    }

    public void trameChargementStart(){bluetoothSerial.write("StartLoading", true);
    }

}

