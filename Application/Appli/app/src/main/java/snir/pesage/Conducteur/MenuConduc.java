package snir.pesage.Conducteur;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.NetworkInterface;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import snir.pesage.Database.BDDManager;
import snir.pesage.Database.Utilisateur;
import snir.pesage.R;


public class MenuConduc extends AppCompatActivity {



    private Button boutonCharg;
    private Button boutonSauv;
    private TextView texteSauv;
    private EditText textNom;
    private EditText textSociete;
    private int nombre = 0;
    private int nombre2 = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_conduc);

        boutonCharg = findViewById(R.id.buttonValider);
        boutonSauv = findViewById(R.id.buttonSauvegarde);
        textNom = findViewById(R.id.editTextNom);
        textSociete = findViewById(R.id.editTextSociete);
        texteSauv = findViewById(R.id.textViewSauvegarde);

        checkUtil();


    }

    public void buttonCharge(View v) {
        if (textNom.getText().toString().trim().length() != 0) {
            if (textSociete.getText().toString().trim().length() != 0) {
                sauvegardeNom();
                openCharge();
            }
        }
        if ((textNom.getText().toString().trim().length() == 0) || (textSociete.getText().toString().trim().length() == 0))
            Snackbar.make(v, "Entrer un nom et une société !", Snackbar.LENGTH_LONG).show();
    }

    private void openCharge() {
        Intent intentCharge = new Intent(this, Chargement.class);
        intentCharge.putExtra("etatTech", false);
        startActivity(intentCharge);
    }

    public void buttonSauv(View v) {
        load();
    }



    private void checkUtil() {
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        Utilisateur utilisateurFromBDD = new Utilisateur();
        utilisateurFromBDD = bdd.getLastUtilisateur("Conducteur");
        if (utilisateurFromBDD != null) {
            nombre = utilisateurFromBDD.getId();
            boutonSauv.setVisibility(View.VISIBLE);
            texteSauv.setVisibility(View.VISIBLE);
        }

        Utilisateur utilisateurFromBDD2 = new Utilisateur();
        utilisateurFromBDD2 = bdd.getLastUtilisateur("Technicien");
        if (utilisateurFromBDD2 != null) {
            nombre2 = utilisateurFromBDD2.getId();
            boutonSauv.setVisibility(View.VISIBLE);
            texteSauv.setVisibility(View.VISIBLE);
        }
        if ( nombre < nombre2) nombre = nombre2;
        bdd.close();
    }

    private void sauvegardeNom(){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();

        Utilisateur utilisateur = new Utilisateur(nombre + 1,textNom.getText().toString(),textSociete.getText().toString(),"Conducteur");
        utilisateur.setEnvoyer(1);
        bdd.insertUtilisateur(utilisateur);
        bdd.close();

    }

    public void load(){
        BDDManager bdd = new BDDManager(this);
        bdd.open();
        Utilisateur utilisateur = new Utilisateur();
        utilisateur = bdd.getLastUtilisateur("Conducteur");

        textNom.setText(utilisateur.getNom());
        textSociete.setText(utilisateur.getSociete());
        bdd.close();
    }

}
