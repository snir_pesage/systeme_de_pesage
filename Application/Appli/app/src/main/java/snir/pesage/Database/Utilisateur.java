package snir.pesage.Database;

/**
 * Created by Kevin on 08/02/2018.
 */

public class Utilisateur {

        private int id;
        private String nom;
        private String societe;
        private String type_utilisateur;
        private String materiaux;
        private int chargeU;
        private String date;
        private int envoyer;
        private String mac;

        public Utilisateur(){}

        public Utilisateur(int id, String nom, String societe, String type_utilisateur){
            this.id = id;
            this.nom = nom;
            this.societe = societe;
            this.type_utilisateur = type_utilisateur;
        }

        public Utilisateur(int id, String nom, String societe, String type_utilisateur, String materiaux, int chargeU, String date, int envoyer, String mac) {
            this.id = id;
            this.nom = nom;
            this.societe = societe;
            this.type_utilisateur = type_utilisateur;
            this.materiaux = materiaux;
            this.chargeU = chargeU;
            this.date = date;
            this.envoyer = 0;
            this.mac = mac;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public String getSociete() {
            return societe;
        }

        public void setSociete(String societe) {
            this.societe = societe;
        }

        public String getType_utilisateur() {
            return type_utilisateur;
        }

        public void setType_utilisateur(String type_utilisateur) {
            this.type_utilisateur = type_utilisateur;
        }

        public String getMateriaux() {
        return materiaux;
    }

        public void setMateriaux(String materiaux) {
            this.materiaux = materiaux;
        }

        public int getChargeU() {
            return chargeU;
        }

        public void setChargeU(int chargeU) {
            this.chargeU = chargeU;
        }

        public String getDate() {
        return date;
    }

        public void setDate(String date) {
        this.date = date;
    }

        public int getEnvoyer() {
        return envoyer;
    }

        public void setEnvoyer(int Envoyer) {
        this.envoyer = Envoyer;
    }

        public String getMac() {
        return mac;
    }

        public void setMac(String mac) {
        this.mac = mac;
    }

}

