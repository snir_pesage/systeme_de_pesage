package snir.pesage.Technicien;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.macroyau.blue2serial.BluetoothSerial;
import com.macroyau.blue2serial.BluetoothSerialListener;

import snir.pesage.Bluetooth.Bluetooth;
import snir.pesage.Bluetooth.Connexion;
import snir.pesage.Database.BDDManager;
import snir.pesage.R;
import snir.pesage.Database.UtilisateurTech;

import static android.os.SystemClock.sleep;

public class TestRelais extends AppCompatActivity {


    protected boolean mbActive;
    protected ProgressBar barreCharg;


    private int nombre;
    private int i;
    private String p;
    private Boolean buttonSwitch = false;

    private String relais1On;
    private String relais2On;
    private String relais3On;
    private String relais4On;

    private String relais1Off;
    private String relais2Off;
    private String relais3Off;
    private String relais4Off;

    private Button buttonRelais1;
    private Button buttonRelais2;
    private Button buttonRelais3;
    private Button buttonRelais4;

    private TextView recu;


    private BluetoothSerial bluetoothSerial;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_relais);

        buttonRelais1 = findViewById(R.id.buttonRelais1);
        buttonRelais2 = findViewById(R.id.buttonRelais2);
        buttonRelais3 = findViewById(R.id.buttonRelais3);
        buttonRelais4 = findViewById(R.id.buttonRelais4);

        recu = findViewById(R.id.textViewRecuRelais);

        barreCharg = findViewById(R.id.progressBarRelais);
        bluetoothSerial = Bluetooth.bluetoothSerial;

        checkUtil();

        new Thread(new Runnable() {
            public void run() {
                while (true){
                    runOnUiThread(new Runnable() {
                        public void run() {
                            recu.setText("Recu : " + Connexion.message);
                        }
                    });
                    if (i == 1) {


                        if ((p.equals(recu.getText().toString())) != true )
                            testRelais1ON();
                            mbActive = false;
                    }
                    if (i == 2){


                        if ((p.equals(recu.getText().toString())) != true ){
                            testRelais1Off();
                            resultatRelais1();
                            mbActive = false;
                        }
                    }
                    if (i == 3){


                        if ((p.equals(recu.getText().toString())) != true ){
                            testRelais2ON();
                            mbActive = false;
                        }
                    }
                    if (i == 4){


                        if ((p.equals(recu.getText().toString())) != true ){
                            testRelais2Off();
                            resultatRelais2();
                            mbActive = false;
                        }
                    }
                    if (i == 5){


                        if ((p.equals(recu.getText().toString())) != true ){
                            testRelais3ON();
                            mbActive = false;
                        }
                    }
                    if (i == 6){


                        if ((p.equals(recu.getText().toString())) != true ){
                            testRelais3Off();
                            resultatRelais3();
                            mbActive = false;
                        }
                    }
                    if (i == 7){


                        if ((p.equals(recu.getText().toString())) != true ){
                            testRelais4ON();
                            mbActive = false;
                        }
                    }
                    if (i == 8){


                        if ((p.equals(recu.getText().toString())) != true ){
                            testRelais4Off();
                            resultatRelais4();
                            mbActive = false;
                        }
                    }
                    p = recu.getText().toString();
                    sleep(100);
                }
            }
        }).start();

    }







    public void testRelais1(View v){
        if (mbActive == false) {

            if (buttonSwitch == true) {
                bluetoothSerial.write("rel1OFF", true);

                i = 2;

                buttonRelais1.setEnabled(true);
                buttonRelais2.setEnabled(true);
                buttonRelais3.setEnabled(true);
                buttonRelais4.setEnabled(true);


                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonRelais1.setText("Demarrer");
                        barreCharg.setVisibility(View.INVISIBLE);
                    }
                });


                mbActive = true;
                buttonSwitch = false;

            }else{

                mbActive = true;

                buttonRelais1.setEnabled(true);
                buttonRelais2.setEnabled(false);
                buttonRelais3.setEnabled(false);
                buttonRelais4.setEnabled(false);
                bluetoothSerial.write("rel1ON", true);
                i = 1;

                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonRelais1.setText("Arreter");
                        barreCharg.setVisibility(View.VISIBLE);
                    }
                });

                buttonSwitch = true;
            }


        }

    }

    public void testRelais2(View v){
        if (mbActive == false) {

            if (buttonSwitch == true) {
                bluetoothSerial.write("rel2OFF", true);

                i = 4;

                buttonRelais1.setEnabled(true);
                buttonRelais2.setEnabled(true);
                buttonRelais3.setEnabled(true);
                buttonRelais4.setEnabled(true);


                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonRelais1.setText("Demarrer");
                        barreCharg.setVisibility(View.INVISIBLE);
                    }
                });


                mbActive = true;
                buttonSwitch = false;

            }else{

                mbActive = true;

                buttonRelais1.setEnabled(false);
                buttonRelais2.setEnabled(true);
                buttonRelais3.setEnabled(false);
                buttonRelais4.setEnabled(false);
                bluetoothSerial.write("rel2ON", true);
                i = 3;

                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonRelais1.setText("Arreter");
                        barreCharg.setVisibility(View.VISIBLE);
                    }
                });

                buttonSwitch = true;
            }


        }

    }

    public void testRelais3(View v){
        if (mbActive == false) {

            if (buttonSwitch == true) {
                bluetoothSerial.write("rel3OFF", true);

                i = 6;

                buttonRelais1.setEnabled(true);
                buttonRelais2.setEnabled(true);
                buttonRelais3.setEnabled(true);
                buttonRelais4.setEnabled(true);


                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonRelais3.setText("Demarrer");
                        barreCharg.setVisibility(View.INVISIBLE);
                    }
                });


                mbActive = true;
                buttonSwitch = false;

            }else{

                mbActive = true;

                buttonRelais1.setEnabled(false);
                buttonRelais2.setEnabled(false);
                buttonRelais3.setEnabled(true);
                buttonRelais4.setEnabled(false);
                bluetoothSerial.write("rel3ON", true);
                i = 5;

                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonRelais3.setText("Arreter");
                        barreCharg.setVisibility(View.VISIBLE);
                    }
                });

                buttonSwitch = true;
            }


        }
    }

    public void testRelais4(View v){
        if (mbActive == false) {

            if (buttonSwitch == true) {
                bluetoothSerial.write("rel4OFF", true);

                i = 8;

                buttonRelais1.setEnabled(true);
                buttonRelais2.setEnabled(true);
                buttonRelais3.setEnabled(true);
                buttonRelais4.setEnabled(true);


                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonRelais4.setText("Demarrer");
                        barreCharg.setVisibility(View.INVISIBLE);
                    }
                });


                mbActive = true;
                buttonSwitch = false;

            }else{

                mbActive = true;

                buttonRelais1.setEnabled(false);
                buttonRelais2.setEnabled(false);
                buttonRelais3.setEnabled(false);
                buttonRelais4.setEnabled(true);
                bluetoothSerial.write("rel4ON", true);
                i = 7;

                runOnUiThread(new Runnable() {
                    public void run() {
                        buttonRelais4.setText("Arreter");
                        barreCharg.setVisibility(View.VISIBLE);
                    }
                });

                buttonSwitch = true;
            }


        }
    }

    private void resultatRelais1(){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurTech = new UtilisateurTech();
        utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
        utilisateurTech.setTest_relais1(relais1On + relais1Off);
        utilisateurTech.setEnvoyer(0);
        bdd.updateUtilisateurTech(nombre, utilisateurTech);
        bdd.close();
    }

    private void resultatRelais2(){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurTech = new UtilisateurTech();
        utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
        utilisateurTech.setTest_relais2(relais2On + relais2Off);
        utilisateurTech.setEnvoyer(0);
        bdd.updateUtilisateurTech(nombre, utilisateurTech);
        bdd.close();
    }

    private void resultatRelais3(){
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurTech = new UtilisateurTech();
        utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
        utilisateurTech.setTest_relais3(relais3On + relais3Off);
        utilisateurTech.setEnvoyer(0);
        bdd.updateUtilisateurTech(nombre, utilisateurTech);
        bdd.close();
    }

    private void resultatRelais4(){

        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurTech = new UtilisateurTech();
        utilisateurTech = bdd.getLastUtilisateurTech("Technicien");
        utilisateurTech.setTest_relais4(relais4On + relais4Off);
        utilisateurTech.setEnvoyer(0);
        bdd.updateUtilisateurTech(nombre, utilisateurTech);
        bdd.close();

    }

    public void testRelais1ON(){
        if ( Connexion.message.equals("rel1ONOK")) {
            relais1On = "ON=OK / ";
        }
        else relais1On = "ON=FAIL / ";

    }

    public void testRelais2ON(){
        if ( Connexion.message.equals("rel2ONOK") ) {
            relais2On = "ON=OK / ";
        }
        else relais2On = "ON=FAIL / ";
    }

    public void testRelais3ON(){
        if ( Connexion.message.equals("rel3ONOK")) {
            relais3On = "ON=OK / ";
        }
        else relais3On = "ON=FAIL / ";
    }

    public void testRelais4ON(){
        if ( Connexion.message.equals("rel4ONOK") ) {
            relais4On = "ON=OK / ";
        }
        else relais4On = "ON=FAIL / ";
    }

    public void testRelais1Off(){
        if ( Connexion.message.equals("rel1OFFOK") ) {
            relais1Off = "OFF=OK";
        }
        else relais1Off = "OFF=FAIL";

    }

    public void testRelais2Off(){
        if ( Connexion.message.equals("rel2OFFOK") ) {
            relais2Off = "OFF=OK";
        }
        else relais2Off = "OFF=FAIL";
    }

    public void testRelais3Off(){
        if ( Connexion.message.equals("rel3OFFOK") ) {
            relais3Off = "OFF=OK";
        }
        else relais3Off = "OFF=FAIL";
    }

    public void testRelais4Off(){
        if ( Connexion.message.equals("rel4OFFOK") ) {
            relais4Off = "OFF=OK";
        }
        else relais4Off = "OFF=FAIL";
    }

    private void checkUtil() {
        BDDManager bdd =  new BDDManager(this);
        bdd.open();
        UtilisateurTech utilisateurFromBDD = new UtilisateurTech();
        utilisateurFromBDD = bdd.getLastUtilisateurTech("Technicien");
        if ( utilisateurFromBDD != null) nombre = utilisateurFromBDD.getId();
        bdd.close();
    }
}
