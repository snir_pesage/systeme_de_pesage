package snir.pesage.Database;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Kevin on 05/03/2018.
 */


public class JSONParser {

    String charset = "UTF-8";
    HttpURLConnection conn;
    DataOutputStream wr;
    StringBuilder result;
    URL urlObj;
    JSONObject jObj = null;
    StringBuilder sbParams;
    String paramsString;

    public JSONParser() {
    }

    public JSONObject makeHttpRequest(String url, String method, HashMap<String, String> params) {
        this.sbParams = new StringBuilder();
        int i = 0;

        for(Iterator var6 = params.keySet().iterator(); var6.hasNext(); ++i) {
            String key = (String)var6.next();

            try {
                if(i != 0) {
                    this.sbParams.append("&");
                }

                this.sbParams.append(key).append("=").append(URLEncoder.encode((String)params.get(key), this.charset));
            } catch (UnsupportedEncodingException var11) {
                var11.printStackTrace();
            }
        }

        if(method.equals("POST")) {
            try {
                this.urlObj = new URL(url);
                this.conn = (HttpURLConnection)this.urlObj.openConnection();
                this.conn.setDoOutput(true);
                this.conn.setRequestMethod("POST");
                this.conn.setRequestProperty("Accept-Charset", this.charset);
                this.conn.setReadTimeout(10000);
                this.conn.setConnectTimeout(15000);
                this.conn.connect();
                this.paramsString = this.sbParams.toString();
                this.wr = new DataOutputStream(this.conn.getOutputStream());
                this.wr.writeBytes(this.paramsString);
                this.wr.flush();
                this.wr.close();
            } catch (IOException var10) {
                var10.printStackTrace();
            }
        } else if(method.equals("GET")) {
            if(this.sbParams.length() != 0) {
                url = url + "?" + this.sbParams.toString();
            }

            try {
                this.urlObj = new URL(url);
                this.conn = (HttpURLConnection)this.urlObj.openConnection();
                this.conn.setDoOutput(false);
                this.conn.setRequestMethod("GET");
                this.conn.setRequestProperty("Accept-Charset", this.charset);
                this.conn.setConnectTimeout(15000);
                this.conn.connect();
            } catch (IOException var9) {
                var9.printStackTrace();
            }
        }

        try {
            InputStream in = new BufferedInputStream(this.conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            this.result = new StringBuilder();

            String line;
            while((line = reader.readLine()) != null) {
                this.result.append(line);
            }
        } catch (IOException var12) {
            var12.printStackTrace();
        }

        this.conn.disconnect();

        try {
            this.jObj = new JSONObject(this.result.toString());
        } catch (JSONException var8) {

        }

        return this.jObj;
    }
}
