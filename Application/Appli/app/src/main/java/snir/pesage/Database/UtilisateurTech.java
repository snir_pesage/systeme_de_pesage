package snir.pesage.Database;

/**
 * Created by Kevin on 14/02/2018.
 */

public class UtilisateurTech {

    private int id;
    private String nom;
    private String type_utilisateur;
    private String test_moteur_ON;
    private String test_moteur_OFF;
    private String test_elec_tension;
    private String test_elec_intensite;
    private String test_elec_puissance;
    private String test_elec_energie;
    private String test_demarrage_ON;
    private String test_demarrage_OFF;
    private String test_demarrage_AU;
    private String test_mesure;
    private String test_mesureAV;
    private String test_relais1;
    private String test_relais2;
    private String test_relais3;
    private String test_relais4;
    private String date;
    private int envoyer;
    private String mac;

    public UtilisateurTech(){}

    public UtilisateurTech(int id, String nom, String type_utilisateur){
        this.id = id;
        this.nom = nom;
        this.type_utilisateur = type_utilisateur;
    }

    public UtilisateurTech(int id, String nom, String type_utilisateur, String test_moteur_ON, String test_moteur_OFF, String test_elec_tension,
                           String test_elec_intensite, String test_elec_puissance, String test_elec_energie, String test_demarrage_ON,
                           String test_demarrage_OFF, String test_demarrage_AU, String test_mesure, String test_mesureAV, String test_relais1,
                           String test_relais2, String test_relais3, String test_relais4, String date, int envoyer, String mac) {
        this.id = id;
        this.nom = nom;
        this.type_utilisateur = type_utilisateur;
        this.test_moteur_ON = test_moteur_ON;
        this.test_moteur_OFF = test_moteur_OFF;
        this.test_elec_tension = test_elec_tension;
        this.test_elec_intensite = test_elec_intensite;
        this.test_elec_puissance = test_elec_puissance;
        this.test_elec_energie = test_elec_energie;
        this.test_demarrage_ON = test_demarrage_ON;
        this.test_demarrage_OFF = test_demarrage_OFF;
        this.test_demarrage_AU = test_demarrage_AU;
        this.test_mesure = test_mesure;
        this.test_mesureAV = test_mesureAV;
        this.test_relais1 = test_relais1;
        this.test_relais2 = test_relais2;
        this.test_relais3 = test_relais3;
        this.test_relais4 = test_relais4;
        this.date = date;
        this.envoyer = 0;
        this.mac = mac;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType_utilisateur() {
        return type_utilisateur;
    }

    public void setType_utilisateur(String type_utilisateur) {
        this.type_utilisateur = type_utilisateur;
    }

    public String getTest_moteur_ON() {
        return test_moteur_ON;
    }

    public void setTest_moteur_ON(String test_moteur_ON) {
        this.test_moteur_ON = test_moteur_ON;
    }

    public String getTest_moteur_OFF() {
        return test_moteur_OFF;
    }

    public void setTest_moteur_OFF(String test_moteur_OFF) {
        this.test_moteur_OFF = test_moteur_OFF;
    }

    public String getTest_elec_tension() {
        return test_elec_tension;
    }

    public void setTest_elec_tension(String test_elec_tension) {
        this.test_elec_tension = test_elec_tension;
    }

    public String getTest_elec_intensite() {
        return test_elec_intensite;
    }

    public void setTest_elec_intensite(String test_elec_intensite) {
        this.test_elec_intensite = test_elec_intensite;
    }

    public String getTest_elec_puissance() {
        return test_elec_puissance;
    }

    public void setTest_elec_puissance(String test_elec_puissance) {
        this.test_elec_puissance = test_elec_puissance;
    }

    public String getTest_elec_energie() {
        return test_elec_energie;
    }

    public void setTest_elec_energie(String test_elec_energie) {
        this.test_elec_energie = test_elec_energie;
    }

    public String getTest_demarrage_ON() {
        return test_demarrage_ON;
    }

    public void setTest_demarrage_ON(String test_demarrage_ON) {
        this.test_demarrage_ON = test_demarrage_ON;
    }

    public String getTest_demarrage_OFF() {
        return test_demarrage_OFF;
    }

    public void setTest_demarrage_OFF(String test_demarrage_OFF) {
        this.test_demarrage_OFF = test_demarrage_OFF;
    }

    public String getTest_demarrage_AU() {
        return test_demarrage_AU;
    }

    public void setTest_demarrage_AU(String test_demarrage_AU) {
        this.test_demarrage_AU = test_demarrage_AU;
    }

    public String getTest_mesure() {
        return test_mesure;
    }

    public void setTest_mesure(String test_mesure) {
        this.test_mesure = test_mesure;
    }

    public String getTest_mesureAV() {
        return test_mesureAV;
    }

    public void setTest_mesureAV(String test_mesureAV) {
        this.test_mesureAV = test_mesureAV;
    }

    public String getTest_relais1() {
        return test_relais1;
    }

    public void setTest_relais1(String test_relais1) {
        this.test_relais1 = test_relais1;
    }

    public String getTest_relais2() {
        return test_relais2;
    }

    public void setTest_relais2(String test_relais2) {
        this.test_relais2 = test_relais2;
    }

    public String getTest_relais3() {
        return test_relais3;
    }

    public void setTest_relais3(String test_relais3) {
        this.test_relais3 = test_relais3;
    }

    public String getTest_relais4() {
        return test_relais4;
    }

    public void setTest_relais4(String test_relais4) {
        this.test_relais4 = test_relais4;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getEnvoyer() {
        return envoyer;
    }

    public void setEnvoyer(int Envoyer) {
        this.envoyer = Envoyer;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

}
