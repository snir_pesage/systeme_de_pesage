package com.example.testunitaire;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private TextView resultat;
    private Button sauvegarder;
    private Button charger;
    private Button continuer;

    private EditText nom;

    private int nombre = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultat = findViewById(R.id.textView);
        sauvegarder = findViewById(R.id.button);
        charger = findViewById(R.id.button2);
        continuer = findViewById(R.id.button3);
        nom = findViewById(R.id.editText);

        checkUtil();
    }

    public void save(View v) {
        BDDManager bdd = new BDDManager(this);
        bdd.open();

        Utilisateur utilisateur = new Utilisateur(nombre+1,nom.getText().toString(),"","Conducteur");
        bdd.insertUtilisateur(utilisateur);

        bdd.close();

        checkUtil();
    }

    public void load(View v){
        Context context = getApplicationContext();

        BDDManager bdd = new BDDManager(this);
        bdd.open();

        Utilisateur utilisateurFromBDD = new Utilisateur();
        utilisateurFromBDD = bdd.getLastUtilisateur("Conducteur");

        if (utilisateurFromBDD != null) {
            resultat.setText(utilisateurFromBDD.getNom());
        }
        else {
            Toast toast = Toast.makeText(context, "Nom introuvable", Toast.LENGTH_SHORT);
            toast.show();
        }
        bdd.close();
    }

    public void continuer(View v){
        Intent intentTest = new Intent(this, PageTest.class);
        startActivity(intentTest);
    }


    private void checkUtil() {
        BDDManager bdd = new BDDManager(this);
        bdd.open();

        Utilisateur utilisateurFromBDD = new Utilisateur();
        utilisateurFromBDD = bdd.getLastUtilisateur("Conducteur");

        if (utilisateurFromBDD != null) {
            nombre = utilisateurFromBDD.getId();
        }

        bdd.close();
    }
}
