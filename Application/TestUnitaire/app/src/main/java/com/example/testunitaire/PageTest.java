package com.example.testunitaire;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PageTest extends AppCompatActivity {


    private TextView resultat;
    private int nombre = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_test);

        resultat = findViewById(R.id.textView2);
    }

    public void load(View v){
        Context context = getApplicationContext();
        BDDManager bdd = new BDDManager(this);
        bdd.open();
        Utilisateur utilisateurFromBDD = new Utilisateur();
        utilisateurFromBDD = bdd.getLastUtilisateur("Conducteur");
        if (utilisateurFromBDD != null) {
            resultat.setText("Bonjour " + utilisateurFromBDD.getNom() + " de la société " + utilisateurFromBDD.getSociete());
        }
        else {
            Toast toast = Toast.makeText(context, "Nom introuvable", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    public void save(View v) {
        BDDManager bdd = new BDDManager(this);
        bdd.open();

        Utilisateur utilisateur = new Utilisateur(nombre+1,"Test","Pesage","Conducteur","Ble",10,100,300,"10/01/2018",1,"0C:21:54:A2:6F:87");
        bdd.insertUtilisateur(utilisateur);
        nombre +=1;
        bdd.close();

        checkUtil();
    }

    private void checkUtil() {
        BDDManager bdd = new BDDManager(this);
        bdd.open();

        Utilisateur utilisateurFromBDD = new Utilisateur();
        utilisateurFromBDD = bdd.getLastUtilisateur("Conducteur");

        if (utilisateurFromBDD != null) {
            nombre = utilisateurFromBDD.getId();
        }

        bdd.close();
    }

    public void delete(View v){
        Context context = getApplicationContext();
        BDDManager bdd = new BDDManager(this);
        bdd.open();
        Utilisateur utilisateurFromBDD = new Utilisateur();
        utilisateurFromBDD = bdd.getLastUtilisateur("Conducteur");
        if (utilisateurFromBDD != null) {
            bdd.supprimerUtilisateur(String.valueOf(nombre));
        }
        else {
            Toast toast = Toast.makeText(context, "Il n'y a plus rien", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

}

