package com.example.testunitaire;

/**
 * Created by Kevin on 29/03/2018.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;



public class BDDManager {

    private static final int VERSION_BDD = 2;
    private static final String NOM_BDD = "utilisateur.db";

    private static final String TABLE_IDENTIFIANT = "table_identifiant";
    private static final String COL_ID = "id";
    private static final int NUM_COL_ID = 0;
    private static final String COL_NOM = "NOM";
    private static final int NUM_COL_NOM = 1;
    private static final String COL_SOCIETE = "SOCIETE";
    private static final int NUM_COL_SOCIETE = 2;
    private static final String COL_TYPE = "TYPE";
    private static final int NUM_COL_TYPE = 3;
    private static final String COL_MATERIAUX = "MATERIAUX";
    private static final int NUM_COL_MATERIAUX = 4;
    private static final String COL_POIDSAV = "POIDSAV";
    private static final int NUM_COL_POIDSAV = 5;
    private static final String COL_POIDSM = "POIDSM";
    private static final int NUM_COL_POIDSM = 6;
    private static final String COL_CHARGEU= "CHARGEU";
    private static final int NUM_COL_CHARGEU = 7;
    private static final String COL_DATE= "DATE";
    private static final int NUM_COL_DATE = 8;
    private static final String COL_ENVOYER = "ENVOYER";
    private static final int NUM_COL_ENVOYER = 9;
    private static final String COL_MAC = "MAC";
    private static final int NUM_COL_MAC = 10;



    private SQLiteDatabase bdd;

    private BDD maBaseSQLite;

    public BDDManager(Context context){
        //On crée la BDD et sa table
        maBaseSQLite = new BDD(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = maBaseSQLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    public long insertUtilisateur(Utilisateur utilisateur){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_ID, utilisateur.getId());
        values.put(COL_NOM, utilisateur.getNom());
        values.put(COL_SOCIETE, utilisateur.getSociete());
        values.put(COL_TYPE, utilisateur.getType_utilisateur());
        values.put(COL_MATERIAUX, utilisateur.getMateriaux());
        values.put(COL_POIDSAV, utilisateur.getPoidsAV());
        values.put(COL_POIDSM, utilisateur.getPoidsM());
        values.put(COL_CHARGEU, utilisateur.getChargeU());
        values.put(COL_DATE, utilisateur.getDate());
        values.put(COL_ENVOYER, utilisateur.getEnvoyer());
        values.put(COL_MAC, utilisateur.getMac());
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_IDENTIFIANT, null, values);
    }



    public int updateUtilisateur(int id, Utilisateur utilisateur){

        ContentValues values = new ContentValues();
        values.put(COL_NOM, utilisateur.getNom());
        values.put(COL_SOCIETE, utilisateur.getSociete());
        values.put(COL_TYPE, utilisateur.getType_utilisateur());
        values.put(COL_MATERIAUX, utilisateur.getMateriaux());
        values.put(COL_POIDSAV, utilisateur.getPoidsAV());
        values.put(COL_POIDSM, utilisateur.getPoidsM());
        values.put(COL_CHARGEU, utilisateur.getChargeU());
        values.put(COL_DATE, utilisateur.getDate());
        values.put(COL_ENVOYER, utilisateur.getEnvoyer());
        values.put(COL_MAC, utilisateur.getMac());
        return bdd.update(TABLE_IDENTIFIANT, values, COL_ID + " = " + id, null);
    }




    public Utilisateur getLastUtilisateur(String type){

        Cursor c = bdd.query(TABLE_IDENTIFIANT, new String[] {COL_ID, COL_NOM, COL_SOCIETE, COL_TYPE, COL_MATERIAUX, COL_POIDSAV, COL_POIDSM,
                COL_CHARGEU, COL_DATE, COL_ENVOYER, COL_MAC}, COL_TYPE + " LIKE \"" + type +"\"", null, null, null, COL_ID +" DESC", "1");
        return cursorToUtilisateur(c);
    }

    public Utilisateur getLastUtilisateurEnvoyer(int envoyer){

        Cursor c = bdd.query(TABLE_IDENTIFIANT, new String[] {COL_ID, COL_NOM, COL_SOCIETE, COL_TYPE, COL_MATERIAUX, COL_POIDSAV, COL_POIDSM,
                COL_CHARGEU, COL_DATE, COL_ENVOYER, COL_MAC}, COL_ENVOYER + " LIKE \"" + envoyer +"\"", null, null, null, COL_ID +" DESC", "1");
        return cursorToUtilisateur(c);
    }


    public void supprimerUtilisateur(String id) {
        bdd.delete(TABLE_IDENTIFIANT, COL_ID + " = ?", new String[] {id});
    }



    //Cette méthode permet de convertir un cursor en un livre
    private Utilisateur cursorToUtilisateur(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();

        Utilisateur utilisateur = new Utilisateur();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        utilisateur.setId(c.getInt(NUM_COL_ID));
        utilisateur.setNom(c.getString(NUM_COL_NOM));
        utilisateur.setSociete(c.getString(NUM_COL_SOCIETE));
        utilisateur.setType_utilisateur(c.getString(NUM_COL_TYPE));
        utilisateur.setMateriaux(c.getString(NUM_COL_MATERIAUX));
        utilisateur.setPoidsAV(c.getInt(NUM_COL_POIDSAV));
        utilisateur.setPoidsM(c.getInt(NUM_COL_POIDSM));
        utilisateur.setChargeU(c.getInt(NUM_COL_CHARGEU));
        utilisateur.setDate(c.getString(NUM_COL_DATE));
        utilisateur.setEnvoyer(c.getInt(NUM_COL_ENVOYER));
        utilisateur.setMac(c.getString(NUM_COL_MAC));
        //On ferme le cursor
        c.close();


        return utilisateur;
    }

}
