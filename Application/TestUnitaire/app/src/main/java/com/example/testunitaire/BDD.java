package com.example.testunitaire;

/**
 * Created by Kevin on 29/03/2018.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDD extends SQLiteOpenHelper {

    private static final String TABLE_IDENTIFIANT = "table_identifiant";




    private static final String CREATE_BDD = "CREATE TABLE table_identifiant ( id INT PRIMARY KEY NOT NULL, nom TEXT, " +
            "societe TEXT, type TEXT, materiaux TEXT, poidsAV NUMBER, poidsM NUMBER, chargeU NUMBER, date TEXT, envoyer NUMBER, mac TEXT)";



    public BDD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //on crée la table à partir de la requête écrite dans la variable CREATE_BDD
        db.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE " + TABLE_IDENTIFIANT + ";");
        onCreate(db);
    }


}
